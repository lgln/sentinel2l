from minio import Minio
import os


class Storage:

    def __init__(self, url, a_key, s_key):
        global mc
        mc = Minio(url, access_key=a_key, secret_key=s_key, secure=True)


    def list_buckets(self, ):
        buckets = mc.list_buckets()
        for bucket in buckets:
            print(bucket.name, bucket.creation_date)


    def list_bucket(self, bucket):
        if mc.bucket_exists(bucket):
            print(f'Bucket {bucket} exists')
            objects = mc.list_objects(bucket)
            for object in objects:
                print(object)
        else:
            print(f'Bucket {bucket} does not exist')


    def store(self, file_path, object_name, bucket):
        if mc.bucket_exists(bucket):
            mc.fput_object(bucket, object_name, file_path)
        else:
            print(f'Bucket {bucket} does not exist')
            
    
    def object_exist(self, bucket_name, object_name):
        try:
            mc.stat_object(bucket_name, object_name)
        except:
            return False
        return True