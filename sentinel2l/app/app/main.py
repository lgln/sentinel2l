import os, sys, json
import argparse
import geopandas as gpd
from pathlib import Path
from minio import Minio
from minio.error import S3Error
import rasterio
from loguru import logger

from dotenv import load_dotenv
load_dotenv('.env')
from config import settings

sys.path.append('app')
import functions, tiling, mosaicing, plot, db


def arg_parser():
    # Arguments
    parser = argparse.ArgumentParser()

    # where
    parser.add_argument(
        "--area",
        type=str,
        help="A district of germany",
    )
    
    parser.add_argument(
        "--s2_name",
        type=str, # CHANGE str or list
        help="Create new tiles for S2 tile",
    )

    # when
    parser.add_argument(
        "--date_str",
        type=str,
        help="Date in the format yyyymmdd. E.g. 2020 or 20221231",
    )

    # what
    parser.add_argument(
        "--task",
        choices=['list-s2-tiles', 'tiling', 'list-tiles', 'lcc-inferencing', 'plotting', 'ndvi', 'get-landkreise', 'list-landkreise-with-tiles', 'list-landkreise', 'mosaicing'],
        type=str, # CHANGE str or list
        help="Task",
        required=True,
    )
    parser.add_argument(
        "--channel",
        type=str,
        help="Channel",
        default="TCI_10m"
    )
    return parser.parse_args()


def main(args):
    
    # LOADING
    logger.info(f'Starting Sentinel2l')
    logger.info(f'with the following settings: {settings}')
    
    if args.task in('list-s2-tiles', 'tiling'):
        # Sentinel-2 tiles
        s2_tiles = gpd.read_file(f'{settings.COS_PUB_URL}sentinel2l-data/DE_S2.geojson')
        s2_tiles['name'] = s2_tiles['s2_name']
        s2_tiles.set_index('name', inplace=True)
        s2_names = s2_tiles.s2_name.values

        name, be_name, selected_area, selected_tiles = functions.load_tiles(area=args.area, allow_overlap=True)
        

    if args.task == 'list-s2-tiles':
        
        s2_names_for_area = []
        for s2_name in s2_names:
                be_in_s2 = gpd.sjoin(selected_tiles, 
                             s2_tiles.loc[[s2_name]], 
                             how='inner', 
                             predicate='within')
                if be_in_s2.shape[0]:
                    s2_names_for_area.append(s2_name)
        json.dump(s2_names_for_area, sys.stdout)
        
        
    if args.task == 'tiling':
        s2_name = args.s2_name
        channel = args.channel
        date_str = args.date_str
        
        logger.info(f'Taks: Tiling')
        logger.info(f'Area: {args.area}')
        logger.info(f'Date: {date_str}')
        logger.info(f'S2 Name: {s2_name}')
        logger.info(f'Channel: {channel}')
        tmp_path = Path(settings.TMP_PATH)
        cloud_percent_acceptance = 5

        be_in_s2 = gpd.sjoin(selected_tiles, 
                     s2_tiles.loc[[s2_name]], 
                     how='inner', 
                     predicate='within')

        be_in_s2['nr'] = be_in_s2[be_name]
        be_in_s2.set_index('nr', inplace=True)

        if be_in_s2.shape[0]:
            print(f'Processing {s2_name} with {be_in_s2.shape[0]} tiles')

            L2A_folders = tiling.list_L2A_folders_s3(settings, 'Sentinel-2/MSI/L2A', s2_name, date_str)

            for L2A_folder in L2A_folders:
                tiling.create_cloudfree_tiles(settings, cloud_percent_acceptance, be_in_s2, L2A_folder['s2_path'], channel, Path(tmp_path), storage_path=None)

                
    if args.task == 'get-landkreise':
        area = args.area
        date_str = args.date_str
        channel = args.channel

        logger.info(f'Taks: Get Landkreise')
        logger.info(f'Area: {area}')
        logger.info(f'Date: {date_str}')
        logger.info(f'Channel: {channel}')
        
        date = functions.date_str2date(date_str)

        body = {"date": f'{date}',
                "area": f'{area}',
                "data_type": f'{channel}'
               }
        
        response = db.post(settings, 
                           endpoint = 'get_landkreise', 
                           body = body)
        area_list = response.json()

        json.dump(area_list, sys.stdout)       
    
    
    if args.task == 'list-landkreise':
        
        name, be_name, selected_area, selected_tiles = functions.load_tiles(area=args.area, allow_overlap=False)
        
        lk_list = selected_area.nuts3_no_umlaute.to_list()

        json.dump(lk_list, sys.stdout)
        
        
    if args.task == 'list-tiles':

        name, be_name, selected_area, selected_tiles = functions.load_tiles(area=args.area, allow_overlap=True)

        tile_list = []
        for tile_nr in selected_tiles.index:
            tile_list = tile_list + plot.list_tiles_for_inferencing(settings, tile_nr=tile_nr, date_str=args.date_str)

        with open('/tmp/tiles.json', 'w') as f:
            json.dump(tile_list, f)
        #json.dump(tile_list, sys.stdout)
        
        
    if args.task == 'list-landkreise-with-tiles':
        
        new_tiles = json.loads(os.environ["TILES"])

        area = gpd.read_file(f'{settings.COS_PUB_URL}geojson/landkreise.geojson')
        tiles = gpd.read_file(f'{settings.COS_PUB_URL}geojson/DE_BE8.geojson')

        intersect = gpd.sjoin(area, tiles, how = 'inner')

        areas_with_new_tiles = []
        for area_name, area_name_noumlaute in zip(area.nuts3_simple_name, area.nuts3_no_umlaute):
            tiles_in_area = intersect.loc[intersect.nuts3_simple_name == area_name].be8_nr.tolist()
            if any(item[:10] in tiles_in_area for item in new_tiles):
                areas_with_new_tiles.append(area_name_noumlaute)

        with open('/tmp/landkreise.json', 'w') as f:
            json.dump(areas_with_new_tiles, f)
        # json.dump(areas_with_new_tiles, sys.stdout, ensure_ascii=False)
        
        
    if args.task == 'plotting':
        pass # TODO
        

    if args.task == 'ndvi':

        area = args.area
        date_str = args.date_str

        date = functions.date_str2date(date_str)

        logger.info(f'Taks: Processing NDVI')
        logger.info(f'Area: {area}')
        logger.info(f'Date: {date}')
        tmp_path = Path(settings.TMP_PATH)

        channel_list = ['B04_10m', 'B08_10m']

        body = {"date": f'{date}',
        "area": f'{area}',
        "data_type": channel_list
        }
        response = db.post(settings,
                           endpoint = 'get_be_tiles',
                           body = body)
        be_tile_list = response.json()

        for tile_nr in be_tile_list:
            plot.ndvi_plot_s3(settings, tmp_path, tile_nr=tile_nr, date_str=date_str)
    


    if args.task == 'mosaicing':

        area = args.area
        date_str = args.date_str
        channel = args.channel

        date = functions.date_str2date(date_str)

        logger.info(f'Taks: Processing NDVI')
        logger.info(f'Area: {area}')
        logger.info(f'Date: {date}')
        logger.info(f'Channel: {channel}')

        mc_mosaics = Minio(settings.COS_ENDPOINT_URL, 
           access_key = settings.COS_ACCESS_KEY_ID.get_secret_value(),
           secret_key = settings.COS_SECRET_ACCESS_KEY.get_secret_value())
        mosaics_bucket = settings.LANDKREISE_BUCKET
    
        mosaics_path = Path('./data/mosaics_tmp')
        
        name, be_name, selected_area, selected_tiles = functions.load_tiles(area=area, allow_overlap=False)

        selected_tiles, all_dates, tiles_count = functions.available_tiles(settings, 
                                                         selected_tiles,
                                                         date_str=date_str, 
                                                         data_type='S2',
                                                         channel_list=['TCI_10m'],
                                                         tmp_folder_path=None)
        for date in all_dates:

            cogeo_path, geojson_path = mosaicing.create_mosaic_from_bucket(settings,
                                                                           mosaics_path, 
                                                                           selected_tiles,
                                                                           data_type='S2',
                                                                           image_date=date, 
                                                                           mosaic_name=name,
                                                                           channel=channel,
                                                                           max_days_back=0,
                                                                           crop_area=selected_area.geometry,
                                                                           res=10,
                                                                           save_used_dates=False)
            # Upload COG and GeoJSON
            if cogeo_path:
                if settings.COS_ENDPOINT_URL:
                    landkreis_dict = {"name": name,
                                    "date": f'{date[0:4]}-{date[4:6]}-{date[6:8]}',
                                    "data_type": channel,
                                    "url": settings.COS_PUB_URL + mosaics_bucket + '/' + cogeo_path.name
                                   }
                    response = db.s2_to_db(settings,
                                               table_name = 'landkreise',
                                               data = [landkreis_dict])

                logger.info(f'Uploading {cogeo_path.name}')
                mc_mosaics.fput_object(bucket_name=mosaics_bucket, object_name=cogeo_path.name,
                                file_path=cogeo_path)
            if False and geojson_path:
                mc_mosaics.fput_object(bucket_name=mosaics_bucket, object_name=geojson_path.name,
                                    file_path=geojson_path)

        
if __name__ == '__main__':
    args = arg_parser()
    main(args)
