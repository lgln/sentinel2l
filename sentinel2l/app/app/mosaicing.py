from pathlib import Path

import rasterio
from rasterio.mask import mask
from rasterio.merge import merge
from rasterio.enums import Resampling
from rasterio.transform import Affine
from rasterio.warp import calculate_default_transform, reproject, Resampling

from tqdm import tqdm
import os
from rio_cogeo.cogeo import cog_translate
from rio_cogeo.profiles import cog_profiles

from minio import Minio
import fnmatch

import numpy as np
import pandas as pd
import geopandas as gpd
import locale
#locale.setlocale(locale.LC_ALL, 'de_DE.utf8')

import datetime
from loguru import logger


def nearest_lower(dates, image_date):
    if int(image_date) >= int(min(dates)):
        dates = sorted(dates)
        date = datetime.datetime.strptime(str(image_date), '%Y%m%d')
        l = [date - datetime.datetime.strptime(str(i), '%Y%m%d') for i in dates]
        return dates[l.index(min(i for i in l if i >= datetime.timedelta(0)))]
    else:
        return '19700101'


def get_dates_from_list(image_list):
    dates = []
    for file_name in image_list:
        dates.append(file_name.split('_')[-3]) # TODO Warnin. mosaic name must have _
    return dates


def transform_to_crs(src_path, dst_path, dst_crs):
    
    with rasterio.open(src_path) as src:
        transform, width, height = calculate_default_transform(
            src.crs, dst_crs, src.width, src.height, *src.bounds)
        kwargs = src.meta.copy()
        kwargs.update({
            'crs': dst_crs,
            'transform': transform,
            'width': width,
            'height': height
        })

        with rasterio.open(dst_path, 'w', **kwargs) as dst:
            for i in range(1, src.count + 1):
                reproject(
                    source=rasterio.band(src, i),
                    destination=rasterio.band(dst, i),
                    src_transform=src.transform,
                    src_crs=src.crs,
                    dst_transform=transform,
                    dst_crs=dst_crs,
                    resampling=Resampling.bilinear)


def cloud_optize_geotiff(src_path, dst_path, cog_profile='jpeg'):

    output_profile = cog_profiles.get(cog_profile)

    config = dict(
        GDAL_NUM_THREADS="ALL_CPUS",
        GDAL_TIFF_INTERNAL_MASK=True
    )

    cog_translate(
        str(src_path),
        str(dst_path),
        output_profile,
        config=config,
        in_memory=False,
        quiet=True
    )


def read_image_in_res(image_path, resolution=10):
    
    with rasterio.open(image_path) as src:

        old_height = src.shape[0]
        old_width = src.shape[1]

        old_res = src.meta['transform'][0]
        resampling_factor = old_res / resolution
        res_diff = resolution - old_res
        left = src.meta['transform'][2]
        top = src.meta['transform'][5]

        new_width = int(old_width * resampling_factor)
        new_height = int(old_height * resampling_factor)

        old_shape = src.shape # no artefacts
        #print(f'old {old_shape}')
        new_shape = (3, new_height, new_width) # artefacts
        #print(f'new:{new_shape}')


        image_data = src.read(out_shape=old_shape  # TODO check for compresseion artefacts
                              )
        image_meta = src.meta.copy()
        #print(image_meta)
        transform = Affine(resolution, 0.0, left-res_diff/2, # fixed the shifting 
                           0.0, -resolution, top+res_diff/2) # TODO check this! 
        image_meta.update({"width": new_width,
                           "height": new_height,
                           "transform": transform}
                          )
        #print(image_meta)
        return image_data, image_meta


def crop_raster(in_raster_path, area, out_raster_path, create_png=False):
    with rasterio.open(in_raster_path) as raster:
        
        area = area.to_crs(raster.crs)

        cropped_raster, out_trans = rasterio.mask.mask(raster, area, crop=True)

        mask, mask_trans, window = rasterio.mask.raster_geometry_mask(raster, area, crop=True)

        profile = raster.profile
        profile.update({"width": cropped_raster.shape[2],
                            "height": cropped_raster.shape[1],
                            "transform": out_trans,
                            'nodata': None})

        with rasterio.Env(GDAL_TIFF_INTERNAL_MASK=True):
            with rasterio.open(out_raster_path, "w", **profile) as cropped_raster_file:
                cropped_raster_file.write(cropped_raster)
                cropped_raster_file.write_mask(np.invert(mask))
                
        if create_png:
            profile.update({"nodata": 0,
                    "nodatavals": (0,0,0)}
                   )
            png_path = Path(str(in_raster_path)[:-3] + 'png')
            profile.update({"driver": "PNG"})
            with rasterio.open(png_path, "w", **profile) as output:
                output.write(cropped_raster)

                
def add_meta_data(image_path, meta_data):
    with rasterio.open(image_path, "r+") as image: #
        image.update_tags(**meta_data)


def mosaic(mosaic_tiles, mosaic_path):
    # Merge tiles to mosaic
    mosaic_result, out_trans = merge(mosaic_tiles, method='max') #max removes gaps between tiles, caused by black pixels

    height = mosaic_result.shape[1]
    width = mosaic_result.shape[2]
    one_of_the_tiles = rasterio.open(mosaic_tiles[0])
    out_profile = one_of_the_tiles.profile
    out_profile.update({"width": width,
                        "height": height,
                        "transform": out_trans}
                       )
    with rasterio.open(mosaic_path, "w", **out_profile) as mosaic_file:
        mosaic_file.write(mosaic_result)
    return


def create_mosaic_from_bucket(settings, mosaics_path, be_tiles, data_type='S2', image_date='20200623',
                  mosaic_name='Mosaik', channel='TCI_10m', max_days_back=0, 
                  crop_area=None, res=None, save_used_dates=True):
    
    # rasterio need this AWS env vars:
    os.environ["AWS_S3_ENDPOINT"]=str(settings.COS_ENDPOINT_URL)
    os.environ["AWS_ACCESS_KEY_ID"]=settings.COS_ACCESS_KEY_ID.get_secret_value()
    os.environ["AWS_SECRET_ACCESS_KEY"]=settings.COS_SECRET_ACCESS_KEY.get_secret_value()
    os.environ["AWS_VIRTUAL_HOSTING"]="FALSE"
    os.environ["AWS_HTTPS"]="YES"

    # Select tiles for mosaic
    oldest_date = image_date

    for be_nr in tqdm(be_tiles.index, desc=f'Reading tiles from bucket: {image_date}'):

        prefix = f'{be_nr}/{data_type}'
        url = settings.COS_PUB_URL + settings.TILES_BUCKET + f'/?prefix={prefix}&format=json'
        content = pd.read_json(url)

        objects_in_bucket = content.name.to_list()

        pattern = f'*/{data_type}/*/{be_nr}_{data_type}_*_{channel}.tif'
        image_list = fnmatch.filter(objects_in_bucket, pattern)

        dates = get_dates_from_list(image_list) # TODO Warning: tile name must have '_'

        if len(dates):
            nearest_date = str(nearest_lower(dates, int(image_date)))

            days_back = (datetime.datetime.strptime(image_date, '%Y%m%d') - datetime.datetime.strptime(nearest_date, '%Y%m%d')).days

            if days_back <= max_days_back:

                # Open tile from bucket and add to mosaic_tiles
                file_name = f'{be_nr}_{data_type}_{nearest_date}_{channel}.tif'
                tile_url = f'{settings.COS_PUB_URL}/{settings.TILES_BUCKET}/{be_nr}/{data_type}/{nearest_date}/{file_name}'

                be_tiles.at[be_nr, 'tile_url'] = tile_url

                # Update dataframe for GeoJSON writing in mosaic function
                be_tiles.at[be_nr, 'used_date'] = nearest_date

                if nearest_date < oldest_date:
                    oldest_date = nearest_date

            else:
                logger.info(f'{be_nr}: Missing tile in mosaic. Try to increase max_days_back')
                return None, None
        else:
            logger.info(f'{be_nr}: No data available for this tile')
            return None, None            
        
    if oldest_date != image_date:
        image_date = oldest_date + '-' + image_date
        
    utm_zone_with_max_tiles = be_tiles.groupby(['utm_zone']).count()['be8_nr'].idxmax()
    mosaic_crs = f'EPSG:326{utm_zone_with_max_tiles}'
    
    max_tiles = be_tiles.loc[be_tiles['utm_zone'] == utm_zone_with_max_tiles]
    mosaic_tiles = list(max_tiles.tile_url)
    
    mosaic_folder_path = mosaics_path / mosaic_name

    mosaic_folder_path.mkdir(parents=True, exist_ok=True)
    main_mosaic_path = mosaic_folder_path / f'{utm_zone_with_max_tiles}_{mosaic_name}_{image_date}_S2_{channel}.tif'
    
    logger.info(f'Creating {mosaic_name} mosaic ({image_date})...')
    mosaic(mosaic_tiles, main_mosaic_path)

    other_utm_tiles = be_tiles.loc[be_tiles['utm_zone'] != utm_zone_with_max_tiles]

    if other_utm_tiles.empty:
        out_raster_path = main_mosaic_path
    else:
        # Create Mosaics for other UTM Zones
        mosaic_paths = [str(main_mosaic_path)]

        for utm_zone in other_utm_tiles.utm_zone.unique():

            tiles = be_tiles.loc[be_tiles['utm_zone'] == utm_zone]
            mosaic_tiles = list(tiles.tile_url)

            mosaic_path = mosaic_folder_path / f'{utm_zone}_{mosaic_name}_{image_date}_S2_{channel}.tif'
            logger.info(f'Creating {mosaic_name} mosaic ({image_date}) additional UTM zone...')
            mosaic(mosaic_tiles, mosaic_path)

            transformed_mosaic_path = mosaics_path / f'{utm_zone}_to_{utm_zone_with_max_tiles}_{mosaic_name}_{image_date}_S2.tif'
            logger.info(f'Transform mosaic...')
            transform_to_crs(mosaic_path, transformed_mosaic_path, mosaic_crs)

            mosaic_paths.append(str(transformed_mosaic_path))

        # Merge the Mosaics:
        with rasterio.open(main_mosaic_path) as main_mosaic:

            logger.info(f'Merging mosaics...')
            final_mosaic_result, out_trans = merge(mosaic_paths, method='max')

            height = final_mosaic_result.shape[1]
            width = final_mosaic_result.shape[2]

            out_profile = main_mosaic.profile
            out_profile.update({"width": width,
                                "height": height,
                                "transform": out_trans}
                               )

            mosaic_folder_path = mosaics_path / mosaic_name
            mosaic_folder_path.mkdir(parents=True, exist_ok=True)

            final_mosaic_path =  mosaic_folder_path / f'{mosaic_name}_{image_date}_S2.tif'

            with rasterio.open(final_mosaic_path, "w", **out_profile) as final_mosaic_file:
                final_mosaic_file.write(final_mosaic_result)
                
        out_raster_path = final_mosaic_path
            
    # Change resolution
    if res is not None:
        in_raster_path = out_raster_path

        out_raster_path = mosaic_folder_path / f'mosaic_{mosaic_name}_{image_date}_S2_{channel}_{res}.tif'

        image_data, image_meta = read_image_in_res(in_raster_path, resolution=res)
        
        with rasterio.open(out_raster_path, "w", **image_meta) as dst:
            dst.write(image_data)
        os.remove(in_raster_path)
    
    # Crop raster data to an area
    if crop_area is not None:
        in_raster_path = out_raster_path
        out_raster_path = mosaic_folder_path / f'cropped_{mosaic_name}_{image_date}_S2_{channel}.tif'
        
        crop_raster(in_raster_path, crop_area, out_raster_path)
        os.remove(in_raster_path)

    # Add meta data
    add_meta = True
    if add_meta:

        date_time = f'{image_date[0:4]}:{image_date[4:6]}:{image_date[6:8]} 00:00:00'
                
        meta_data = {}
        meta_data['AREA_NAME'] = mosaic_name
        meta_data['DATETIME_START'] = date_time
        meta_data['DATETIME_END'] = date_time #TODO
        meta_data['GSD'] = res
        
        meta_data['MISSION'] = 'Sentinel-2'
        meta_data['INSTRUMENT'] = 'MSI'
        meta_data['PROCESSING_LEVEL'] = 'L2A'
        meta_data['DATATYPE'] = channel
        
        meta_data['PROVIDER_PRODUCER'] = 'European Union/ESA/Copernicus'
        meta_data['PROVIDER_PRODUCER_URL'] = 'https://sentinel.esa.int/web/sentinel/user-guides/sentinel-2-msi'
        meta_data['PROVIDER_LICENSOR'] = 'European Union/ESA/Copernicus'
        meta_data['PROVIDER_LICENSOR_URL'] = 'https://sentinel.esa.int/web/sentinel/user-guides/sentinel-2-msi'
        meta_data['PROVIDER_PROCESSOR'] = 'Landesamt für Geoinformation und Landesvermessung Niedersachsen (LGLN)'
        meta_data['PROVIDER_PROCESSOR'] = 'LGLN'
        meta_data['PROVIDER_PROCESSOR_URL'] = 'https://www.lgln.niedersachsen.de'
        
        meta_data['TIFFTAG_DATETIME'] = date_time
        meta_data['TIFFTAG_COPYRIGHT'] = f'Creative Commons (CC BY 4.0), Contains  modified  Copernicus  Sentinel  data  {image_date[0:4]}, processed at Landesamt für Geoinformation und Landesvermessung Niedersachsen (LGLN)'
        
        add_meta_data(out_raster_path, meta_data)
        
    # Create Cloud Optimized GeoTiff 
    cloud_optimize = True
    if cloud_optimize:
        
        in_raster_path = out_raster_path
        
        if channel == 'TCI_10m':
            out_raster_path =  mosaic_folder_path / f'{mosaic_name}_{image_date}_S2.tif'
        elif channel == 'C08_10m':
            out_raster_path =  mosaic_folder_path / f'{mosaic_name}_{image_date}_S2-NIR.tif'
        elif channel == 'NDVI_10m':
            out_raster_path =  mosaic_folder_path / f'{mosaic_name}_{image_date}_S2-NDVI.tif'            
        else:
            out_raster_path =  mosaic_folder_path / f'{mosaic_name}_{image_date}_S2-{channel}.tif'
        
        if channel == 'NDV_10m' or channel == 'NDW_10m':
            cloud_optize_geotiff(in_raster_path, out_raster_path, cog_profile='deflate')
        else:
            cloud_optize_geotiff(in_raster_path, out_raster_path, cog_profile='jpeg')
        
        os.remove(in_raster_path)

    if save_used_dates:
        geojson_path = mosaic_folder_path / f'{mosaic_name}_{image_date}_S2.geojson'
        be_tiles.to_file(geojson_path, driver='GeoJSON')
    else:
        geojson_path = None

    return out_raster_path, geojson_path
        
                 
def create_mosaic(storage_path, mosaics_path, be_tiles, data_type='S2', image_date='20200623',
                  mosaic_name='Mosaik', channel='TCI_10m', max_days_back=0, crop_area=None, res=10):
    # Select tiles for mosaic
    mosaic_tiles = []

    for be_nr in tqdm(be_tiles.index, desc=f'Reading tiles from storage: {image_date}'):

        path = storage_path / be_nr / data_type
        if path.exists() and data_type == 'S2':
            dates = [int(x.name) for x in os.scandir(path)]
            nearest_date = str(nearest_lower(dates, int(image_date)))
            days_back = (datetime.datetime.strptime(image_date, '%Y%m%d') - datetime.datetime.strptime(nearest_date, '%Y%m%d')).days

            if days_back <= max_days_back:

                file_name = f'{be_nr}_{data_type}_{nearest_date}_{channel}.tif'

                be_tiles.at[be_nr, 'used_date'] = nearest_date
                be_image_path = storage_path / be_nr / data_type / nearest_date / file_name

                be_image = rasterio.open(be_image_path, "r")
                mosaic_tiles.append(be_image)
                mosaic_crs.append(be_image.crs)
            else:
                logger.info(f'{be_nr}: Missing tile in mosaic. Try to increase max_days_back')
                return None, None
        elif path.exists() and data_type == 'multitemporal':
            file_name = f'{be_nr}_{image_date}_{channel}.tif'
            be_image_path = storage_path / be_nr / data_type / image_date / file_name
            be_image = rasterio.open(be_image_path, "r")
            mosaic_tiles.append(be_image)
        else:
            logger.warning(f'{be_nr}: no S2 folder')
            return None, None

    #Create the mosaic
    mosaic_path, geojson_path = mosaic(be_tiles, mosaic_tiles, image_date, mosaics_path, mosaic_name=mosaic_name, channel=channel, crop_area=crop_area, res=res)
    return mosaic_path, geojson_path