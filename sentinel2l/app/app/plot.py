import os
from tqdm import tqdm
import rasterio
import numpy as np
import matplotlib.pyplot as plt
from skimage import img_as_ubyte
from rasterio.plot import reshape_as_raster, reshape_as_image, show
from minio import Minio
import fnmatch
from pathlib import Path
import functions, db

def get_colors(inp, colormap, vmin=None, vmax=None):
    norm = plt.Normalize(vmin, vmax)
    return colormap(norm(inp))[0]


def plot(in_path, out_path, cm=plt.cm.viridis , min_value=0, max_value=255):
    in_image = rasterio.open(in_path)
    in_profile = in_image.profile
    in_data = in_image.read()

    out_profile = in_profile
    out_profile.update(
        {"count": 3,
        "dtype": 'uint8'})

    out_data = get_colors(in_data, cm, min_value, max_value)

    out_data = img_as_ubyte(out_data[...,:-1])
    
    out_data = reshape_as_raster(out_data)

    with rasterio.open(out_path, "w", **out_profile) as output:
        output.write(out_data)

        
def ndvi(b8_path, b4_path, ndvi_path):

    b8_image = rasterio.open(b8_path)
    nir = b8_image.read().astype('float64')
    
    b4_image = rasterio.open(b4_path)
    red = b4_image.read().astype('float64')
    
    np.seterr(divide='ignore', invalid='ignore')
    ndvi = np.where(
        (nir+red)==0, 
        0, 
        (nir-red)/(nir+red))
    
    out_profile = b8_image.profile
    out_profile.update(
        {"dtype": 'float64'}) # TODO ?

    with rasterio.open(ndvi_path, "w", **out_profile) as output:
        output.write(ndvi) 


def ndwi(b3_path, b8_path, ndwi_path):

    b3_image = rasterio.open(b3_path)
    green = b3_image.read().astype('float64')
    
    b8_image = rasterio.open(b8_path)
    nir = b8_image.read().astype('float64')
    
    np.seterr(divide='ignore', invalid='ignore')
    ndwi = np.where(
        (green+nir)==0, 
        0, 
        (green-nir)/(green+nir))
    
    out_profile = b3_image.profile
    out_profile.update(
        {"dtype": 'float64'}) # TODO ?

    with rasterio.open(ndwi_path, "w", **out_profile) as output:
        output.write(ndwi) 
        
        
def tiff_diff(a_path, b_path, out_path):
    
    a_image = rasterio.open(a_path)
    b_image = rasterio.open(b_path)
    
    a_data = a_image.read().astype('float64')
    b_data = b_image.read().astype('float64')

    out_data = a_data - b_data

    out_profile = a_image.profile
    out_profile.update(
        {"dtype": 'float64'}) # TODO ?

    with rasterio.open(out_path, "w", **out_profile) as output:
        output.write(out_data) 
        
        
def list_tiles(mc_tiles, tiles_bucket, tile_nr='8-17405936', date_str='20', channel='TCI_10m'):

    objects_in_bucket = [b.object_name for b in mc_tiles.list_objects(tiles_bucket, prefix=f'{tile_nr}/S2/{date_str}', recursive=True)]

    pattern = f'{tile_nr}/S2/{date_str}*/{tile_nr}_S2_{date_str}*_*{channel}.tif'
    tiles = fnmatch.filter(objects_in_bucket, pattern)

    return tiles


def list_tiles_for_inferencing(settings, tile_nr='', date_str=''):

    # rasterio need this AWS env vars:
    os.environ["AWS_S3_ENDPOINT"]=str(settings.COS_ENDPOINT_URL)
    os.environ["AWS_ACCESS_KEY_ID"]=settings.COS_ACCESS_KEY_ID.get_secret_value()
    os.environ["AWS_SECRET_ACCESS_KEY"]=settings.COS_SECRET_ACCESS_KEY.get_secret_value()
    os.environ["AWS_VIRTUAL_HOSTING"]="FALSE"
    os.environ["AWS_HTTPS"]="YES"

    mc_tiles = Minio(settings.COS_ENDPOINT_URL,
           access_key = settings.COS_ACCESS_KEY_ID.get_secret_value(),
           secret_key = settings.COS_SECRET_ACCESS_KEY.get_secret_value(), secure=True)
    tiles_bucket = settings.TILES_BUCKET

    file_paths = list_tiles(mc_tiles, tiles_bucket, tile_nr=tile_nr, date_str=date_str)

    tile_list = ['/'.join(file.split('/')[0:3]) for file in file_paths]

    return tile_list


def ndvi_plot_s3(settings, tmp_path, tile_nr='', date_str=''):

    # rasterio need this AWS env vars:
    os.environ["AWS_S3_ENDPOINT"]=str(settings.COS_ENDPOINT_URL)
    os.environ["AWS_ACCESS_KEY_ID"]=settings.COS_ACCESS_KEY_ID.get_secret_value()
    os.environ["AWS_SECRET_ACCESS_KEY"]=settings.COS_SECRET_ACCESS_KEY.get_secret_value()
    os.environ["AWS_VIRTUAL_HOSTING"]="FALSE"
    os.environ["AWS_HTTPS"]="YES"
    
    mc_tiles = Minio(settings.COS_ENDPOINT_URL, 
           access_key = settings.COS_ACCESS_KEY_ID.get_secret_value(),
           secret_key = settings.COS_SECRET_ACCESS_KEY.get_secret_value(), secure=True) 
    tiles_bucket = settings.TILES_BUCKET
    bucket_name = settings.TILES_BUCKET

    tile_list = list_tiles(mc_tiles, tiles_bucket, tile_nr=tile_nr, date_str=date_str, channel='TCI_10m')

    for tci_path in tqdm(tile_list, desc=f'NDVI plots for {tile_nr}:'):

        try:
            # input
            b08_path = 's3://' + tiles_bucket + '/' + tci_path[:-11] + 'B08' + tci_path[-8:]
            b04_path = 's3://' + tiles_bucket + '/' + tci_path[:-11] + 'B04' + tci_path[-8:]

            ndvi_object_name = tci_path[:-11] + 'NDV' + tci_path[-8:]
            tmp_ndvi_path = Path(str(tmp_path) + '/' + ndvi_object_name)
            tmp_ndvi_path.parent.mkdir(parents=True, exist_ok=True)

            # NDVI
            ndvi(b08_path, b04_path, tmp_ndvi_path)

            mc_tiles.fput_object(bucket_name=tiles_bucket,
                                 object_name=ndvi_object_name,
                                 file_path=tmp_ndvi_path)
            os.remove(tmp_ndvi_path)

            ndvi_path = 's3://' + tiles_bucket + '/' + ndvi_object_name
            ndvi_plot_object_name = tci_path[:-11] + 'NDVI' + tci_path[-8:]
            tmp_ndvi_plot_path = Path(str(tmp_path) + '/' + ndvi_plot_object_name)
            tmp_ndvi_plot_path.parent.mkdir(parents=True, exist_ok=True)

            # plot
            plot(ndvi_path, tmp_ndvi_plot_path, plt.cm.RdYlGn, 0, 1)

            object_name = ndvi_plot_object_name

            mc_tiles.fput_object(bucket_name=tiles_bucket,
                                 object_name=object_name,
                                 file_path=tmp_ndvi_plot_path)
            os.remove(tmp_ndvi_plot_path)

            day_str = tci_path[-20:-12]
            be_tile_id = db.get_be_tile_id(settings, tile_nr, day_str)

            be_tile_dict = {"be_tile_stats_id": be_tile_id,
                            "channel_name": 'NDVI',
                            "resolution": '10m',
                            "be_tile_url": f'{settings.COS_PUB_URL}{bucket_name}/{object_name}'
                           }
            db.post(settings, endpoint='s2_to_db', body = {"table_name": 'be_tiles', "data": [be_tile_dict]})

        except:
            print('NDVI failed')
        
        
def ndwi_plot_s3(settings, tmp_path, tile_nr='', date_str=''):

    # rasterio need this AWS env vars:
    os.environ["AWS_S3_ENDPOINT"]=str(settings.COS_ENDPOINT_URL)
    os.environ["AWS_ACCESS_KEY_ID"]=settings.COS_ACCESS_KEY_ID.get_secret_value()
    os.environ["AWS_SECRET_ACCESS_KEY"]=settings.COS_SECRET_ACCESS_KEY.get_secret_value()
    os.environ["AWS_VIRTUAL_HOSTING"]="FALSE"
    os.environ["AWS_HTTPS"]="YES"
    
    mc_tiles = Minio(settings.COS_ENDPOINT_URL, 
           access_key = settings.COS_ACCESS_KEY_ID.get_secret_value(),
           secret_key = settings.COS_SECRET_ACCESS_KEY.get_secret_value(), secure=True) 
    tiles_bucket = settings.TILES_BUCKET

    tile_list = list_tiles(mc_tiles, tiles_bucket, tile_nr=tile_nr, date_str=date_str, channel='TCI_10m')

    for tci_path in tqdm(tile_list, desc=f'NDWI plots for {tile_nr}:'):

        try:
            # input
            b03_path = 's3://' + tiles_bucket + '/' + tci_path[:-11] + 'B03' + tci_path[-8:]
            b08_path = 's3://' + tiles_bucket + '/' + tci_path[:-11] + 'B08' + tci_path[-8:]

            ndwi_object_name = tci_path[:-11] + 'NDW' + tci_path[-8:]
            tmp_ndwi_path = Path(str(tmp_path) + '/' + ndwi_object_name)
            tmp_ndwi_path.parent.mkdir(parents=True, exist_ok=True)

            # NDWI
            ndwi(b03_path, b08_path, tmp_ndwi_path)

            mc_tiles.fput_object(bucket_name=tiles_bucket,
                                 object_name=ndwi_object_name,
                                 file_path=tmp_ndwi_path)
            os.remove(tmp_ndwi_path)

            ndwi_path = 's3://' + tiles_bucket + '/' + ndwi_object_name
            ndwi_plot_object_name = tci_path[:-11] + 'NDWI' + tci_path[-8:]
            tmp_ndwi_plot_path = Path(str(tmp_path) + '/' + ndwi_plot_object_name)
            tmp_ndwi_plot_path.parent.mkdir(parents=True, exist_ok=True)

            # plot
            plot(ndwi_path, tmp_ndwi_plot_path, plt.cm.Blues, -0.1, 0.1)

            mc_tiles.fput_object(bucket_name=tiles_bucket,
                                 object_name=ndwi_plot_object_name,
                                 file_path=tmp_ndwi_plot_path)
            os.remove(tmp_ndwi_plot_path)

        except:
            print('NDWI failed')


def ndvi_plot(storage_path, be_selection, date_str):
    for be_nr in be_selection.index:

        image_list = list(storage_path.glob(f'{be_nr}/S2/{date_str}*/*TCI_10m.tif'))

        for tci_path in tqdm(image_list, desc=f'NDVI plots for {be_nr}:'):

            s = list(tci_path.name)

            s[-11:-8] = 'B08'
            b08_path = tci_path.parent / "".join(s)

            s[-11:-8] = 'B04'
            b04_path = tci_path.parent / "".join(s)

            s = tci_path.name.split('_')
            s[3] = 'NDV'
            ndvi_path = tci_path.parent / '_'.join(s)

            s[3] = 'NDVI'
            ndvi_plot_path = tci_path.parent / '_'.join(s)

            if os.path.isfile(b08_path) and os.path.isfile(b04_path):
                ndvi(b08_path, b04_path, ndvi_path)
                plot(ndvi_path, ndvi_plot_path, plt.cm.RdYlGn, 0, 1)


def color_plot_s3(settings, tmp_path, tile_nr='', date_str='', channel='B08_10m', cm=plt.cm.inferno , min_value=0, max_value=5000):

    # rasterio need this AWS env vars:
    os.environ["AWS_S3_ENDPOINT"]=str(settings.COS_ENDPOINT_URL)
    os.environ["AWS_ACCESS_KEY_ID"]=settings.COS_ACCESS_KEY_ID.get_secret_value()
    os.environ["AWS_SECRET_ACCESS_KEY"]=settings.COS_SECRET_ACCESS_KEY.get_secret_value()
    os.environ["AWS_VIRTUAL_HOSTING"]="FALSE"
    os.environ["AWS_HTTPS"]="YES"
    
    mc_tiles = Minio(settings.COS_ENDPOINT_URL, 
           access_key = settings.COS_ACCESS_KEY_ID.get_secret_value(),
           secret_key = settings.COS_SECRET_ACCESS_KEY.get_secret_value(), secure=True) 
    tiles_bucket = settings.TILES_BUCKET

    tile_list = list_tiles(mc_tiles, tiles_bucket, tile_nr=tile_nr, date_str=date_str, channel=channel)

    for tile_path in tqdm(tile_list, desc=f'Color plots for {tile_nr}:'):

        plot_object_name = tile_path[:-11] + 'C' + tile_path[-10:]

        tmp_plot_path = Path(str(tmp_path) + '/' + plot_object_name)
        tmp_plot_path.parent.mkdir(parents=True, exist_ok=True)

        # plot
        s3_path = 's3://' + tiles_bucket + '/' + tile_path

        plot(s3_path, tmp_plot_path, cm, min_value, max_value)

        mc_tiles.fput_object(bucket_name=tiles_bucket,
                             object_name=plot_object_name,
                             file_path=tmp_plot_path)
        os.remove(tmp_plot_path)


def color_plot(storage_path, be_selection, date_str, channel='B08', cm=plt.cm.inferno , min_value=0, max_value=5000):
    for be_nr in be_selection.index:

        image_list = list(storage_path.glob(f'{be_nr}/S2/{date_str}*/*TCI_10m.tif'))

        for tci_path in tqdm(image_list, desc=f'Color plots for {be_nr}:'):

            s = list(tci_path.name)

            s[-11:-8] = channel
            b_path = tci_path.parent / "".join(s)

            s = list(b_path.name)
            s[-11] = 'C'
            c_path = b_path.parent / "".join(s)

            if os.path.isfile(b_path):
                plot(b_path, c_path, cm, min_value, max_value)
                
        
def ndvi_diff_plot(storage_path, be_selection, dates):
    for be_nr in be_selection.index:

        a_path = list(storage_path.glob(f'{be_nr}/S2/{dates[0]}*/*NDV_10m.tif'))[0]
        b_path = list(storage_path.glob(f'{be_nr}/S2/{dates[1]}*/*NDV_10m.tif'))[0]

        diff_dir = storage_path / be_nr / 'multitemporal' / f'{dates[0]}-{dates[1]}' 
        diff_dir.mkdir(parents=True, exist_ok=True)
        out_path = diff_dir / f'{dates[0]}-{dates[1]}_NDVI_diff.tif'
        plot_path = diff_dir / f'{dates[0]}-{dates[1]}_NDVI_diff.tif'

        if os.path.isfile(a_path) and os.path.isfile(b_path):
            tiff_diff(a_path, b_path, out_path)
            plot(out_path, plot_path, cm=plt.cm.BrBG , min_value=-0.4, max_value=0.4)