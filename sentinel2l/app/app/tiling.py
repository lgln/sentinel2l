import os, json
from pathlib import Path
from shutil import copyfile
import requests
from tqdm import tqdm
import boto3

import numpy as np
import rasterio
from rasterio.mask import mask

import storage, db


def list_L2A_folders_local(s2_path, s2_name, date_str):
    L2A_folders = list(s2_path.glob(f'S2*_MSIL2A_*_T{s2_name}_{date_str}*.SAFE/GRANULE/L2A_T{s2_name}*{date_str}*'))
    return L2A_folders


def list_L2A_folders_codede(s2_path, s2_name, date_str):
    L2A_folders = []

    year_str = date_str[0:4]
    month_str = date_str[4:6]
    day_str = date_str[6:8]

    for year in os.scandir(s2_path):
        if not year_str or year_str == year.name:

            for month in os.scandir(year):
                if not month_str or month_str == month.name:

                    for day in tqdm(os.scandir(month), desc=f'Scanning folders: {year_str} {month_str}'):
                        if not day_str or day_str == day.name:

                            folders = list(Path(day).glob(f'S2*T{s2_name}*.SAFE/GRANULE/L2A*'))
                            if folders:
                                for folder in folders:
                                    L2A_folders.append(folder)
    return L2A_folders


def list_subfolders_in_cos_new(bucket_credentials=None, prefix=''):

    s3_client = boto3.client('s3',
                            aws_access_key_id=bucket_credentials['access_key'], 
                            aws_secret_access_key=bucket_credentials['secret_key'], 
                            endpoint_url="http://"+bucket_credentials['url'])

    paginator = s3_client.get_paginator('list_objects')

    subfolders = []
    for result in paginator.paginate(Bucket=bucket_credentials['bucket'], Delimiter='/', Prefix=prefix):
        try:
            for prefix in result.get('CommonPrefixes'):
                subfolders.append(prefix.get('Prefix'))
        except:
            pass

    return subfolders


def list_subfolders_in_cos(bucket_credentials=None, prefix=''):
    # TODO use Minio
    s3_client = boto3.client('s3',
                           aws_access_key_id=bucket_credentials['access_key'], 
                           aws_secret_access_key=bucket_credentials['secret_key'], 
                           endpoint_url="http://"+bucket_credentials['url'])

    objects_list =  s3_client.list_objects(Prefix=prefix,
                                           Delimiter='/',
                                           Bucket=bucket_credentials['bucket'],
                                           MaxKeys=30000)

    try:
        subfolders = [obj["Prefix"] for obj in objects_list['CommonPrefixes']]
    except:
        subfolders = None

    return subfolders


def list_objs_in_cos(bucket_credentials=None, prefix=''):
    # TODO use Minio
    s3_resource=boto3.resource('s3',
                           aws_access_key_id=bucket_credentials['access_key'], 
                           aws_secret_access_key=bucket_credentials['secret_key'], 
                           endpoint_url="http://"+bucket_credentials['url'])
    bucket_obj = s3_resource.Bucket(bucket_credentials['bucket'])

    objects =  bucket_obj.objects.filter(Prefix=prefix)
    all_objs = [obj.key for obj in objects]
    return all_objs


def list_L2A_folders_s3(settings, s2_path, s2_name, date_str):

    L2A_folders = []

    century_str = date_str[0:2]
    year_str = date_str[2:4]
    month_str = date_str[4:6]
    day_str = date_str[6:8]
    
    bucket_credentials={}
    bucket_credentials['access_key']=settings.CODEDE_ACCESS_KEY_ID.get_secret_value()
    bucket_credentials['secret_key']=settings.CODEDE_SECRET_ACCESS_KEY.get_secret_value()
    bucket_credentials['url']=settings.CODEDE_URL
    bucket_credentials['bucket']=settings.CODEDE_BUCKET

    for year in list_subfolders_in_cos_new(bucket_credentials, prefix=s2_path+'/'+century_str):
        if not year_str or year_str == year[-3:-1]:

            for month in list_subfolders_in_cos_new(bucket_credentials, prefix=year):
                if not month_str or month_str == month[-3:-1]:

                    for day in tqdm(list_subfolders_in_cos_new(bucket_credentials, prefix=month), desc=f'Scanning folders: {year_str} {month_str}'):
                        if not day_str or day_str == day[-3:-1]:

                            safe_folders = list_subfolders_in_cos_new(bucket_credentials, prefix=day)

                            for safe in safe_folders:
                                if s2_name in safe:

                                    L2A_folder_list = list_subfolders_in_cos_new(bucket_credentials, prefix=f'{safe}GRANULE/')
                                    if L2A_folder_list: # if GRANULE folder is empty
                                        L2A_folder = {"s2_id": s2_name, 
                                                      "date": f'{year[-5:-1]}-{month[-3:-1]}-{day[-3:-1]}', 
                                                      "s2_path": L2A_folder_list[0]}
                                        L2A_folders.append(L2A_folder)
    return L2A_folders


def no_clouds(mcp, tci, be_box, max_mcp, meta_data):

    be_mcp, out_transform = rasterio.mask.mask(mcp, be_box, crop=True)
    mc = np.mean(be_mcp)

    if mc > max_mcp:  # check mc > 0 was used for BE8 but to many 0.0 for BE1
        return False # = clouds
        
    tci_pixel, out_transform = rasterio.mask.mask(tci, be_box, crop=True)
    # TODO only blue channel is used. Maybe slow. Find better channel
    white_pixel = (np.count_nonzero(tci_pixel[2] == 255))
    black_pixel = (np.count_nonzero(tci_pixel[2] == 0))

    if black_pixel > 5000 or white_pixel > 500000: # TODO replace this magic numbers
        return False # = large white or black areas
    
    meta_data['MEAN_CLOUD_PROPABILITY'] = mc
    meta_data['BLACK_PIXEL_COUNT'] = black_pixel
    meta_data['WHITE_PIXEL_COUNT'] = white_pixel

    return True # = no clouds


def save_tile(settings, tmp_file_path, be_box, file, meta_data):

    file_str = str('/CODEDE/'+file) # will be used for meta data. '/' matters.

    os.environ["AWS_S3_ENDPOINT"]=settings.CODEDE_URL
    os.environ["AWS_ACCESS_KEY_ID"]=settings.CODEDE_ACCESS_KEY_ID.get_secret_value()
    os.environ["AWS_SECRET_ACCESS_KEY"]=settings.CODEDE_SECRET_ACCESS_KEY.get_secret_value()
    os.environ["AWS_VIRTUAL_HOSTING"]="False"
    os.environ["AWS_HTTPS"]="NO"
    
    with rasterio.open('s3://CODEDE/'+file) as image:
    
        out_image, out_transform = rasterio.mask.mask(image, be_box, crop=True)

        # remove last row with black pixels
        if out_image[:,-1,:].sum() == 0:
            out_image = out_image[:,:-1,:]

        # remove last column with black pixels
        if out_image[:,:,-1].sum() == 0:
            out_image = out_image[:,:,:-1]

        profile = image.profile

        image.close()

        del profile['blockxsize']
        del profile['blockysize']
        profile.update({"driver": "GTiff",
                        "height": out_image.shape[1],
                        "width": out_image.shape[2],
                        "transform": out_transform,
                        "tiled": False},
                       )

        # Add MetaData
        meta_data['DATA_SOURCE'] = file_str.split('/')[1]
        meta_data['MISSION'] = file_str.split('/')[2]
        meta_data['INSTRUMENT'] = file_str.split('/')[3]
        meta_data['PROCESSING_LEVEL'] = file_str.split('/')[4]
        meta_data['PLATFORM'] = file_str.split('/')[8].split('_')[0]
        meta_data['S2_TILE'] = file_str.split('/')[8].split('_')[5][1:6]

        date = file_str.split('/')[8].split('_')[6][0:15]

        date_time = f'{date[0:4]}:{date[4:6]}:{date[6:8]} {date[9:11]}:{date[11:13]}:{date[13:15]}'

        meta_data['PROVIDER_PRODUCER'] = 'European Union/ESA/Copernicus'
        meta_data['PROVIDER_PRODUCER_URL'] = 'https://sentinel.esa.int/web/sentinel/user-guides/sentinel-2-msi'
        meta_data['PROVIDER_LICENSOR'] = 'European Union/ESA/Copernicus'
        meta_data['PROVIDER_LICENSOR_URL'] = 'https://sentinel.esa.int/web/sentinel/user-guides/sentinel-2-msi'
        meta_data['PROVIDER_PROCESSOR'] = 'Landesamt für Geoinformation und Landesvermessung Niedersachsen (LGLN)'
        meta_data['PROVIDER_PROCESSOR'] = 'LGLN'
        meta_data['PROVIDER_PROCESSOR_URL'] = 'https://www.lgln.niedersachsen.de'

        meta_data['TIFFTAG_DATETIME'] = date_time
        meta_data['TIFFTAG_COPYRIGHT'] = f'Creative Commons (CC BY 4.0), Contains  modified  Copernicus  Sentinel  data  {date[0:4]}, processed at Landesamt für Geoinformation und Landesvermessung Niedersachsen (LGLN)'

        with rasterio.open(tmp_file_path, "w", **profile) as output:
            output.write(out_image)
            output.update_tags(**meta_data)
            return True
    return False
    
    
def create_cloudfree_tiles(settings, max_mcp, be_tiles, L2A_folder, channel_str, 
                tmp_path=None, storage_path=None):

    tmp_file_paths = [] # used to remove tmp files
    be_tile_s2_stats_dict_list = [] # for DB writing 
    be_tile_dict_list = [] # for DB writing
    
    date = str(L2A_folder).split('_')[2][:8]

    mcp_p = L2A_folder+'QI_DATA/MSK_CLDPRB_20m.jp2'

    os.environ["AWS_S3_ENDPOINT"]=settings.CODEDE_URL
    os.environ["AWS_ACCESS_KEY_ID"]=settings.CODEDE_ACCESS_KEY_ID.get_secret_value()
    os.environ["AWS_SECRET_ACCESS_KEY"]=settings.CODEDE_SECRET_ACCESS_KEY.get_secret_value()
    os.environ["AWS_VIRTUAL_HOSTING"]="False"
    os.environ["AWS_HTTPS"]="NO"
    
    if not be_tiles.index.empty:
        try:
            with rasterio.open('s3://CODEDE/'+mcp_p) as mcp:
                bucket_credentials={}
                bucket_credentials['access_key']=settings.CODEDE_ACCESS_KEY_ID.get_secret_value()
                bucket_credentials['secret_key']=settings.CODEDE_SECRET_ACCESS_KEY.get_secret_value()
                bucket_credentials['url']=settings.CODEDE_URL
                bucket_credentials['bucket']=settings.CODEDE_BUCKET

                objs = list_objs_in_cos(bucket_credentials,
                                        prefix=L2A_folder+'IMG_DATA/')

                files = []
                for obj in objs:
                    if 'TCI_10m.jp2' in obj:
                        tci_file = obj
                    if f'{channel_str}.jp2' in obj:
                        files.append(obj)

                with rasterio.open('s3://CODEDE/'+tci_file) as tci: # extra image for black and white pixel counting

                    if settings.COS_ENDPOINT_URL:
                        cos = storage.Storage(settings.COS_ENDPOINT_URL, 
                                              settings.COS_ACCESS_KEY_ID.get_secret_value(),
                                              settings.COS_SECRET_ACCESS_KEY.get_secret_value())   
                        bucket_name = settings.TILES_BUCKET

                    for be_nr in tqdm(be_tiles.index, desc=f'Searching cloudfree tiles: {date}'):  # TODO add count as prefix

                        be_tiles = be_tiles.to_crs(tci.crs)

                        be_box = be_tiles.loc[be_tiles.index == be_nr].geometry  # TODO simplify

                        meta_data = {}
                        # Check cloud coverage and if no data
                        # TODO if Database:
                        # TODO get_list_of_mcp from DB (be_nr)
                        # TODO calc_max_mcp(list_of_mcp)
                        if no_clouds(mcp, tci, be_box, max_mcp, meta_data):
                            # write to DB
                            # max 69 entrys per S2 tle

                            be_tile_id = db.get_be_tile_id(settings, be_nr, date)
                            if be_tile_id: # be_tile already exist
                                pass
                            else:
                                data = {
                                        "be_nr": be_nr, 
                                        "date": f'{date[0:4]}-{date[4:6]}-{date[6:8]}',
                                        "mean_cloud_propability": meta_data['MEAN_CLOUD_PROPABILITY'],
                                        "white_pixel_count": meta_data['WHITE_PIXEL_COUNT'],
                                        "black_pixel_count": meta_data['BLACK_PIXEL_COUNT']
                                                   }
                                body = {"table_name": 'be_tile_s2_stats', "data": [data]}

                                response = db.post(settings, endpoint = 's2_to_db', body = body)
                                be_tile_id = response.json()[0]['tile_id']


                            # write files to tmp folder
                            for file in files:
                                
                                channel = file[-11:-4]

                                #count = 0

                                tile_file_name = f'{be_nr}_S2_{date}_{channel}.tif'
                                object_name = f'{be_nr}/S2/{date}/{be_nr}_S2_{date}_{channel}.tif'
                                
                                be_tile_dict = {"be_tile_stats_id": be_tile_id,
                                "channel_name": channel[0:3],
                                "resolution": channel[4:7],
                                "be_tile_url": f'{settings.COS_PUB_URL}{bucket_name}/{object_name}'
                                               }
                                be_tile_dict_list.append(be_tile_dict)
                                
                                tile_is_missing = False

                                
                                # check bucket
                                if settings.COS_ENDPOINT_URL:
                                    if not cos.object_exist(bucket_name, object_name):
                                        tile_is_missing = True

                                # check storage
                                if storage_path:
                                    out_dir = storage_path / str(be_nr) / 'S2' / date
                                    out_path = out_dir / tile_file_name
                                    if not out_path.exists():
                                        tile_is_missing = True

                                # save tile to temp
                                if tile_is_missing:
                                    
                                    tmp_date_dir = tmp_path / str(be_nr) / 'S2' / date
                                    tmp_date_dir.mkdir(parents=True, exist_ok=True)

                                    tmp_file_path = tmp_date_dir / tile_file_name

                                    if save_tile(settings, tmp_file_path, be_box, file, meta_data):
                                        tmp_file_paths.append(tmp_file_path)
        except:
            pass

    # push list for all be_nr (max 69, each with max 36 files):
    db.post(settings, endpoint = 's2_to_db', body = {"table_name": 'be_tiles', "data": be_tile_dict_list})

    if tmp_file_paths:

        # Save to COS
        if settings.COS_ENDPOINT_URL:
            
            for tmp_file_path in tqdm(tmp_file_paths, desc=f'Uploading to COS bucket ({bucket_name})'):
                
                s = str(tmp_file_path).split('/')
                
                be_tile = str(s[-4])
                sensor = str(s[-3])
                date = str(s[-2])
                filename = str(s[-1])
            
                object_name = f'{be_tile}/{sensor}/{date}/{filename}'
                
                cos.store(tmp_file_path, object_name, bucket_name)
            
        # Save to NFS
        if storage_path:

            for tmp_file_path in tqdm(tmp_file_paths, desc=f'Saving tile to storage path ({storage_path})'):

                s = str(tmp_file_path).split('/')
                
                be_tile = str(s[-4])
                sensor = str(s[-3])
                date = str(s[-2])
                filename = str(s[-1])
            
                out_dir = storage_path / be_tile / sensor / date
                out_dir.mkdir(parents=True, exist_ok=True)
                copyfile(tmp_file_path, out_dir / filename)


        # Delete Temp 
        if True:
            for tmp_file_path in tqdm(tmp_file_paths, desc=f'Deleting temp tiles'):
                if os.path.isfile(tmp_file_path):
                    os.remove(tmp_file_path)