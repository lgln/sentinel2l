import geopandas as gpd
from shapely.geometry import box
from tqdm import tqdm
import requests
import pandas as pd
import fnmatch
from datetime import timedelta
import os
from collections import Counter
import numpy as np
from config import settings

def lk2bl(landkreis):
    
    de_lk = gpd.read_file(f'{settings.COS_PUB_URL}sentinel2l-data/landkreise.fgb')
    try:
        bundesland =  de_lk.loc[de_lk.nuts3_no_umlaute == landkreis].nuts1_name.to_list()[0]
    except:
        print(f'Landkreis {landkreis} not found')
        bundesland = None
    return bundesland

def date_str2date(date_str):
    if len(date_str) == 8:
        date = f'{date_str[0:4]}-{date_str[4:6]}-{date_str[6:8]}'
    elif len(date_str) == 6:
        date = f'{date_str[0:4]}-{date_str[4:6]}'
    else:
        date = f'{date_str[0:4]}'
    return date


def load_tiles(area=None, use_total_area=False, be_nrs=None, cx=None, print_list=False, use_cos=False, use_bbox=False, allow_overlap=False, add_buffer=False):
    
    #print(os.environ["ENV_TEST"])
    name = None
    selected_area = None
    selected_tiles = None
    
    be_name = 'be8_nr'
    tiles = gpd.read_file(f'{settings.COS_PUB_URL}sentinel2l-data/DE_BE8.fgb')
    tiles['nr'] = tiles[be_name]
    tiles.set_index('nr', inplace=True)
    
    de_lk = gpd.read_file(f'{settings.COS_PUB_URL}sentinel2l-data/landkreise.fgb')
    
    name = area

    if area == 'Deutschland':
        if use_total_area:
            selected_area = de_lk.dissolve().drop(['nuts3_simple_name', 'nuts3_no_umlaute', 'nuts3_name', 'nuts1_name'], axis=1) 
        else:
            selected_area = de_lk

    else:

        selected_area = de_lk.loc[de_lk.nuts1_name == name] # Bundesland
        
        if selected_area.empty: # Bundesland not found
            selected_area = de_lk.loc[de_lk.nuts3_no_umlaute == name] # Landkreise

            if selected_area.empty: # Landkreis not found
                selected_area = de_lk.loc[de_lk.nuts3_no_umlaute == name] # Landkreise without Umlaut

                if selected_area.empty: # Landkreis not found
                    print('Area not found. Use one of these:')
                    bundesland_liste = de_lk.nuts1_name.unique().tolist()
                    bundesland_liste.sort()
                    print(bundesland_liste)
                    landkreis_liste = de_lk.nuts3_no_umlaute.unique().tolist()
                    landkreis_liste.sort()
                    print(landkreis_liste)
                    
                    name = de_lk.loc[de_lk.nuts3_no_umlaute == name].nuts3_no_umlaute.item() # restore Umlaut

        else: # Area of Bundesland
            if use_total_area:
                selected_area = selected_area.dissolve(by='nuts1_name').drop(['nuts3_simple_name', 'nuts3_no_umlaute', 'nuts3_name'], axis=1)
            else:
                selected_area = selected_area

    if use_bbox:
        bounds = selected_area.total_bounds
        left = bounds[0]
        bottom = bounds[1]
        right = bounds[2]
        top = bounds[3]
        if add_buffer:
            left = left - (-0 * 8000)
            bottom = bottom - (1 * 8000)
            right = right + (0 * 8000)
            top = top + (0 * 8000)
        print(left, bottom, right, top)
        selected_tiles = tiles.cx[left:right, bottom:top]
    else:
        # Use total_area for Bundesland or Deutschland instead of selected_area to avoid duplicated tiles
        if area == 'Deutschland':
            total_area = de_lk.dissolve()
        else:
            total_area = selected_area.dissolve(by='nuts1_name')
        selected_tiles = gpd.sjoin(tiles, total_area, how='inner')


    if selected_tiles.empty:
        if landkreis:
            print(f'Landkreis nicht gefunden:\n{de_lk.name.unique()}')
            return
        if bundesland:
            print(f'Bundesland nicht gefunden:\n{de_bl.name.unique()}')
            return


    #remove columns from sjoin
    selected_tiles = selected_tiles[list(tiles.columns)]

    if not allow_overlap:
        utm_zone_with_max_tiles = selected_tiles.groupby(['utm_zone']).count()['be8_nr'].idxmax()
        #utm_zone_with_max_tiles = '32'
        max_tiles = selected_tiles.loc[selected_tiles['utm_zone'] == utm_zone_with_max_tiles]
        other_utm_tiles = selected_tiles.loc[selected_tiles['utm_zone'] != utm_zone_with_max_tiles]
        area_not_covered = selected_area.overlay(max_tiles, how='difference')

        #remove small areas:
        area_not_covered = area_not_covered.explode(index_parts=True)
        area_not_covered = area_not_covered.loc[area_not_covered.area > 1]

        rest_tiles = gpd.sjoin(other_utm_tiles, 
             area_not_covered,
             how='inner')

        selected_tiles = pd.concat([max_tiles, rest_tiles])
        selected_tiles = selected_tiles[list(tiles.columns)] #remove columns from sjoin

    if cx:
        name = 'Sentinel-2'
        left=cx[0]
        right=cx[1]
        bottom=cx[2]
        top=cx[3]
        selected_tiles = selected_tiles.cx[left:right, bottom:top]

    if be_nrs:
        name = 'Sentinel-2'
        selected_tiles = selected_tiles.loc[be_nrs]
        selected_area = selected_tiles
            
    return name, be_name, selected_area, selected_tiles


def get_dates_from_list(image_list):
    dates = []
    for file_name in image_list:
        dates.append(file_name.split('_')[-3]) # TODO Warnin. mosaic name must have _
    return dates


def available_tiles(settings, 
                    selected_tiles, 
                    date_str='',
                    data_type='S2',
                    channel_list=['TCI_10m'], 
                    tmp_folder_path=None):
    all_dates = []
    tiles_count = 0
    
    for tile_nr in tqdm(selected_tiles.index, desc=f'Lade Kacheln für {date_str}'):
    
        for channel in channel_list:
            # Get content from bucket
            prefix = f'{tile_nr}/{data_type}/{date_str}'
            url = settings.COS_PUB_URL + settings.TILES_BUCKET + f'/?prefix={prefix}&format=json'
            content = pd.read_json(url)

            # Get available dates from file names in bucket
            if len(content):
                objects_in_bucket = content.name.to_list()

                pattern = f'{tile_nr}/{data_type}/{date_str}*/{tile_nr}_{data_type}_{date_str}*_{channel}.tif'
                image_list = fnmatch.filter(objects_in_bucket, pattern)

                if tmp_folder_path:
                     # download the image for each date
                    for image in image_list:
                        image_url = settings.COS_PUB_URL + settings.TILES_BUCKET + '/' + image
                        request = requests.get(image_url)

                        tmp_file_path = tmp_folder_path / image

                        parent_folder=tmp_file_path.parent
                        parent_folder.mkdir(parents=True, exist_ok=True)

                        open(tmp_file_path, 'wb').write(request.content)

                dates = get_dates_from_list(image_list)
                all_dates = all_dates + list(set(dates) - set(all_dates))

            else:
                dates = []

            selected_tiles.at[tile_nr, channel] = int(len(dates))
            tiles_count = tiles_count + int(len(dates))
        
    all_dates = sorted(all_dates)
    
    return selected_tiles, all_dates, tiles_count


# TODO merge with available_tiles ?
def get_class_histogram(settings, 
                    selected_tiles, 
                    date_str='',
                    data_type='Labels',
                    channel='Basis-DLM_10m'):

    class_histogram=Counter({})
    
    for tile_nr in tqdm(selected_tiles.index, desc=f'Lade Kacheln für {date_str}'):
    
        # Get content from bucket
        prefix = f'{tile_nr}/{data_type}/{date_str}'
        url = settings.COS_PUB_URL + settings.TILES_BUCKET + f'/?prefix={prefix}&format=json'
        content = pd.read_json(url)

        # Get available dates from file names in bucket
        if len(content):
            objects_in_bucket = content.name.to_list()

            pattern = f'{tile_nr}/{data_type}/{date_str}*/{tile_nr}_{data_type}_{date_str}*_{channel}.tif'
            image_list = fnmatch.filter(objects_in_bucket, pattern)

             # download the image for each date
            for image_path in image_list:
                image_url = settings.COS_PUB_URL + settings.TILES_BUCKET + '/' + image_path
                with rasterio.open(image_url) as image:
                    data = image.read()
                    k, v = np.unique(data, return_counts=True)
                    d = dict(zip(k, v))
                    class_histogram+=Counter(d)

    class_histogram=dict(sorted(class_histogram.items()))
    
    return class_histogram