import requests
import functions

def post(settings, endpoint = '', body = ''):
    
    db_url = settings.DB_URL
    user_name = settings.DB_BASIC_AUTH_USER_NAME.get_secret_value()
    password = settings.DB_BASIC_AUTH_PASSWORD.get_secret_value()
    url = f"https://{user_name}:{password}@{db_url}/rpc/{endpoint}"
    #print(url)
    #print(body)
    try:
        # Stellen des POST-Requests
        response = requests.post(url, json=body)
    except requests.HTTPError as e:
        status_code = e.response.status_code
        logger.info(f"Fehler beim Speichern in die Datenbank:", status_code)

    return response


def s2_to_db(settings, table_name = '', data = ''):

    obj = {"table_name": table_name, "data": data}
    
    db_url = settings.DB_URL
    user_name = settings.DB_BASIC_AUTH_USER_NAME.get_secret_value()
    password = settings.DB_BASIC_AUTH_PASSWORD.get_secret_value()
    url = f"https://{user_name}:{password}@{db_url}/rpc/s2_to_db"

    try:
        # Stellen des POST-Requests
        response = requests.post(url, json=obj)
    except requests.HTTPError as e:
        status_code = e.response.status_code
        logger.info(f"Fehler beim Speichern in die Datenbank:", status_code)

    return response


def get_be_tile_id(settings, be_nr, date_str):

    db_url = settings.DB_URL
    user_name = settings.DB_BASIC_AUTH_USER_NAME.get_secret_value()
    password = settings.DB_BASIC_AUTH_PASSWORD.get_secret_value()
    
    #date = f'{date_str[0:4]}-{date_str[4:6]}-{date_str[6:8]}'
    date = functions.date_str2date(date_str)
    
    url = f"https://{user_name}:{password}@{db_url}/rpc/get_be_tile_id?be_nr={be_nr}&date={date}"

    try:
        response = requests.get(url)
    except requests.HTTPError as e:
        status_code = e.response.status_code
        logger.info(f"Fehler Lesen der Datenbank:", status_code)

    if response.json():
        be_tile_id = response.json()['id']
    else:
        be_tile_id = None
        
    return be_tile_id