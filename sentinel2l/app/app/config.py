import os
from pathlib import Path
from typing import Optional
from pydantic import SecretStr
from pydantic_settings import BaseSettings
from dotenv import load_dotenv

class Settings(BaseSettings):
    IS_DEBUG:bool = False
    
    # Sentinel2l-PostGIS-DB
    DB_URL: str = "sentinel2-postgrest.ki.lgln.niedersachsen.dev"
    DB_BASIC_AUTH_USER_NAME: SecretStr = 'do_not_save_key_here'
    DB_BASIC_AUTH_PASSWORD: SecretStr = 'do_not_save_key_here'

    # CODE-DE
    CODEDE_URL: str = "data.cloud.code-de.org"
    CODEDE_ACCESS_KEY_ID: SecretStr = 'do_not_save_key_here'
    CODEDE_SECRET_ACCESS_KEY: SecretStr = 'do_not_save_key_here'
    CODEDE_BUCKET: str = "CODEDE"

    # COS Config
    COS_ENDPOINT_URL: str = "cloud.code-de.org:8080"
    COS_ACCESS_KEY_ID: SecretStr = 'do_not_save_key_here'
    COS_SECRET_ACCESS_KEY: SecretStr = 'do_not_save_key_here'
    COS_PUB_URL: str = "https://lgln.community.code-de.org/"

    TILES_BUCKET: str = "be-tiles"
    LANDKREISE_BUCKET: str = "sentinel-2-landkreise"

    TILES_TEST_BUCKET: str = "be-tiles-test"
    LANDKREISE_TEST_BUCKET: str = "sentinel-2-landkreise-test"
        
    TMP_PATH: str = "/tmp"

    class Config:
        case_sensitive = True


settings = Settings()
