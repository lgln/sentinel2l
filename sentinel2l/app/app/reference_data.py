from pathlib import Path
import fnmatch
import matplotlib.pyplot as plt

import numpy as np
import pandas as pd
import geopandas as gpd
import rasterio
from rasterio import features
import fiona
from shapely.geometry import shape, mapping, MultiPolygon
from shapely.ops import cascaded_union

from scipy import stats
import seaborn as sns
import os

from tqdm import tqdm
import storage
import ntpath


def shape_to_geopackage(shapes, gpkg_path, settings):
    """
     - List of shape-files to a geopackage
    """
    for shape in shapes:
        gdf_shape = gpd.read_file(shape)
        layer_name = '_'.join(shape.stem.split('_')[2:])
        gdf_shape.to_file(gpkg_path, layer=layer_name)
    pass


def gpkg_to_tiff(storage_path, tiff_mapping_csv, be8_nr, TN_list, BDLM_date, settings, scale_to=None):
    """
    - Create smaller Basis-DLM tiles
    - Return: object_count
        *
    """
    #TODO Split this Function un multiple

    # this tif defines the area of interest

    endpoint_url = settings.COS_PUB_URL
    bucket = settings.TILES_BUCKET
    prefix = f'{be8_nr}/S2'
    url = endpoint_url + bucket + f'/?prefix={prefix}&format=json'
    content = pd.read_json(url)

    if len(content):
        objects_in_bucket = content.name.to_list()
        pattern = f'{be8_nr}/S2/*/{be8_nr}_S2_*_TCI_10m.tif'
        image_list = fnmatch.filter(objects_in_bucket, pattern)
        
        # this tif defines the area of interest
        tif = image_list[0]

    # create list of shape files
    gpkg_path = Path(f'{storage_path}/{be8_nr}/Shapes/{be8_nr}_{BDLM_date}.gpkg')
    
    # leaf function if a tile do not have any BDLM objects:
    if not gpkg_path.exists():
        return
    else:
        layers_list = []

        for layername in fiona.listlayers(gpkg_path):
            layer_gdf = gpd.read_file(gpkg_path, layer=layername)
            layers_list.append(layer_gdf)

        gdf = gpd.GeoDataFrame(pd.concat(layers_list, ignore_index=True), crs=layer_gdf.crs)

        gdf["TiffID"] = np.nan
        gdf["Bezeichnung"] = ""
        gdf["OBJART"] = pd.to_numeric(gdf["OBJART"])
        gdf = gdf.explode()

        # read class mapping file
        df_cm = pd.read_csv(tiff_mapping_csv)
        # assign ClassID and ClassName from df_classmapping -> class_mapping_file_for_landuse.xlsx
        for grp_oba, grp_dat in df_cm.groupby("Objektart"):
            gdf_sel = gdf.OBJART == grp_oba

            if len(grp_dat) == 1:
                gdf.loc[gdf_sel, "TiffID"] = grp_dat.TiffID.max()      
                gdf.loc[gdf_sel, "Bezeichnung"] = grp_dat.Bezeichnung.max()
            else:
                for row_idx, row in grp_dat.iterrows():
                    if row.Key not in gdf.columns:
                        continue
                    try:
                        if row.Value == "None":
                            subgrp_sel = gdf[row.Key].apply(lambda x: x is None)
                        else:
                            subgrp_sel = gdf[row.Key] == row.Value
                        gdf.loc[gdf_sel & subgrp_sel, "TiffID"] = row.TiffID
                        gdf.loc[gdf_sel & subgrp_sel, "Bezeichnung"] = row.Bezeichnung
                    except KeyError as err:
                        #print(f'Exception: {err}')
                        pass

        gdf = gdf[pd.notna(gdf.TiffID)]
        gdf.TiffID = gdf.TiffID.astype(np.uint8)

        gdf["label_value"] = gdf.TiffID
        # if 0 in gdf.label_value:
        # if 0 is one of the label_values increment all values by 1
        # this is done because 0 is the background color in the label image
        # gdf.label_value += 1

        # here all geometry of the same class are combined by a cascaded_union operation
        class_masks = {}

        # group features in the geodataframe
        for class_id, class_dat in gdf.groupby("TiffID"):
            # generate a multipolygon for each class
            # buffer geometry with 0 buffer to avoid invalid polygons
            class_poly = MultiPolygon([s.buffer(0) for s in class_dat.geometry])

            # run the cascaded_union operation
            class_union = cascaded_union(class_poly)

            class_name = class_dat.Bezeichnung.max()  # get class name as string
            label_value = class_dat.label_value.max()  # get class label

            # store class_name and class_union geometry in the class_masks dictionary
            class_masks[class_id] = (class_id, class_name, label_value, class_union)

        # build a GeoDataFrame from the class_masks dictionary to save them to a shapefile
        gdf_classmasks = gpd.GeoDataFrame(class_masks.values(), index=class_masks.keys(),
                                          columns=["TiffID", "Bezeichnung", "label_value", "geometry"])
        gdf_classmasks.crs = gdf.crs
        outpath = storage_path / be8_nr / "Labels" / BDLM_date
        outpath.mkdir(parents=True, exist_ok=True)

        # save the GeoDataFrame to shapefile
        gpkg_path = outpath / f"{be8_nr}_Labels_{BDLM_date}_Basis-DLM.gpkg"
        gdf_classmasks.to_file(gpkg_path)

        pixel_counts, labels_outpath = write_labels_and_masks(tif, gdf_classmasks, outpath, BDLM_date, settings, plot=False, scale_to=scale_to)

        # Upload to COS
        if True:    

            cos = storage.Storage(settings.COS_ENDPOINT_URL, 
                                  settings.COS_ACCESS_KEY_ID.get_secret_value(),
                                  settings.COS_SECRET_ACCESS_KEY.get_secret_value()) 
            bucket_name = settings.TILES_BUCKET
            
            object_name = f'{be8_nr}/Labels/{BDLM_date}/{labels_outpath.name}'
            cos.store(labels_outpath, object_name, bucket_name)
            #print(f'{settings.COS_PUB_URL}{settings.TILES_BUCKET}/{object_name}')
            
            gpkg_object_name = f'{be8_nr}/Labels/{gpkg_path.name}'
            cos.store(gpkg_path, gpkg_object_name, bucket_name)
            #print(f'{settings.COS_PUB_URL}{settings.TILES_BUCKET}/{gpkg_object_name}')
            

        pixel_counts = pixel_counts[['TiffID', 'Anzahl']]

        df = df_cm.set_index('TiffID').join(pixel_counts.set_index('TiffID'))
        df['Prozent'] = df.Anzahl / df.Anzahl.sum() * 100

        df['Nr'] = df['Objektart'].astype(str).str[:-3].astype(np.int8)

        df.loc[df['Nr'] == 41, 'Objektartengruppe'] = 'Siedlung'
        df.loc[df['Nr'] == 42, 'Objektartengruppe'] = 'Verkehr'
        df.loc[df['Nr'] == 43, 'Objektartengruppe'] = 'Vegetation'
        df.loc[df['Nr'] == 44, 'Objektartengruppe'] = 'Gewässer'

        sns.set()

        colors_24 = ['red', 'orange', 'orange', 'brown', 'grey', 'darkred',
                     'red', 'orange', 'orange', 'brown', 'grey', 'darkred',
                     'yellow', 'indigo', 'lightgreen', 'darkgreen', 'green', 'purple',
                     'brown', 'brown', 'lightblue', 'lightblue', 'blue', 'darkblue']

        colors_4 = ['blue', 'red', 'green', 'yellow']

        df_sum = df.groupby('Objektartengruppe').sum()

        fig = plt.figure(figsize=(10, 20))

        st = fig.suptitle(f"BE-Kachel: {be8_nr} {BDLM_date}", fontsize="x-large")

        ax1 = fig.add_subplot(132)
        ax2 = fig.add_subplot(133)

        ax = df.plot.barh(ax=ax1, x='Bezeichnung', y='Anzahl', color=colors_24)
        ax1.invert_yaxis()
        ax1.set_xlabel('Anzahl 10m x 10m Pixel')
        ax1.set_xlim(0, 40000)
        ax1.get_legend().remove()

        ax = df_sum.plot.pie(ax=ax2, y='Prozent', colors=colors_4, labels=None)
        plt.legend(labels=df_sum.index, loc="upper left")


        TN_stats_plot_path = storage_path / be8_nr / "Labels" / f"TN_{be8_nr}_{BDLM_date}.png"
        fig.savefig(TN_stats_plot_path)

        plots_path = storage_path / 'plots'
        plots_path.mkdir(parents=True, exist_ok=True)
        fig.savefig(plots_path / f"TN_{be8_nr}_{BDLM_date}.png")

        plt.close(fig)


def read_all_shapes(shape_files):
    """
    - Read all shapefiles using fiona.open()
    - Return:
        * shape_geometries (list of geometries)
        * schema, crs, driver from first shapefile
    """

    shape_data = []

    schema, crs, driver = None, None, None

    # iterate the list of shapefiles
    for shp in shape_files:

        # use fiona to read the shapefile
        with fiona.open(shp, "r", driver="ESRI Shapefile") as shape_reader:
            # read schema, crs and driver only once
            # this assumes that those are all the same for all shapefiles
            if schema is None:
                driver = shape_reader.driver
                crs = shape_reader.crs
                schema = shape_reader.schema

            # add geometry and attributes (properties) to a shape_data list
            for rec in shape_reader:
                shape_data.append((rec["geometry"], rec["properties"]))

    return shape_data, schema, crs, driver


def isvalid(geom):
    """
    - Check if Geometry is valid
    """
    try:
        # if this fails geometry is invalid
        shape(geom)
        return 1
    except:
        return 0


def write_labels_and_masks(tif, gdf_classmasks, outpath, BDLM_date, settings, plot=False, scale_to=None):
    """
    - Write class labels and class masks using rasterio
    - Open tif file to get coordinate reference system (crs) and affine transform
    - Rasterize the polygons on the extent of the input image and
      generate a label image
    """
    endpoint_url = settings.COS_PUB_URL
    bucket = settings.TILES_BUCKET

    url = endpoint_url + bucket + '/' + tif
    with rasterio.open(url) as ras:
        # dat = ras.read() # not necessary to read any data values from the raster file
        ras_crs = ras.read_crs()  # extract projection information
        profile = ras.profile  # extract profile (dtyps, shape, etc.)
        # trans = ras.read_transform()  # extract affine transformation

        # reproject if raster has a projection different from shapefiles
        if ras_crs is not None and ras_crs != gdf_classmasks.crs:
            gdf_target = gdf_classmasks.to_crs(ras_crs)
        else:
            gdf_target = gdf_classmasks

        shape_geometries = []
        mask_only_dict = {}
        
        if scale_to is None:
            out_shape = ras.shape
            transform = ras.transform
        else:
            upscale_factor = ras.res[0] / scale_to # e.g. 10m / 5m
            out_shape=(
                int(ras.height * upscale_factor),
                int(ras.width * upscale_factor)
                )

            # scale image transform
            transform = ras.transform * ras.transform.scale(
                (ras.width / out_shape[-1]),
                (ras.height / out_shape[-2])
            )
        
        # group the geodataframe by ClassID
        for class_id, class_dat in gdf_target.groupby("TiffID"):
            class_poly = class_dat.geometry  # multipolygon geometry of the class
            class_val = class_dat.label_value.max()  # label value for each class
            class_name = class_dat.Bezeichnung.max()  # class name as string

            # generate a list of GeoJSON mappings for each polygon in the class multipolygon
            # as well as class_val which is the label value for the label image
            shape_geometries += [(mapping(l), class_val) for l in class_poly]

            # additionally add class_val to a dictorary class_name -> class_val
            mask_only_dict[class_name] = (class_id, class_val)

        # use rasterio's features.rasterize to rasterize the shape_geometries list
        # here the (geometry, class_val) are returned from iterating the list
        # use input image shape and affine transform, this way the resulting
        # label image has the same shape and reference as the input image
        # fill the background with 0 and use dtype uint8
        # the resulting label_gray is a numpy array
        label_gray = features.rasterize(((g, v) for g, v in shape_geometries),
                                        out_shape=out_shape,
                                        transform=transform,
                                        dtype="uint8", fill=0)

         # remove first row with black pixels
        if label_gray[0,:].sum() == 0:
            label_gray = label_gray[1:,:]

        # remove first column with black pixels
        if label_gray[:,0].sum() == 0:
            label_gray = label_gray[:,1:]
        
        
        gray_values, counts = np.unique(label_gray, return_counts=True)
        pixel_counts = pd.DataFrame({'TiffID': gray_values, 'Anzahl': counts})

        if plot:
            # plot the label image
            plt.figure(figsize=(30, 15))
            plt.imshow(label_gray, cmap="gray")
            plt.show()

        # update the input image profile dictionary for the target label image
        profile_gray = dict(profile)
        profile_gray["dtype"] = "uint8"
        profile_gray["count"] = 1
        profile_gray["nodata"] = 255

        # output filename for label image
        if scale_to is None:
            res_ext = tif.split('_')[-1]
        else:
            res_ext = f"{scale_to}m.tif"
            profile_gray["width"] = label_gray.shape[1]
            profile_gray["height"] = label_gray.shape[0]
            profile_gray["transform"] = transform
            
            
            
            
        labels_outpath = outpath / f"{tif[:10]}_Labels_{BDLM_date}_Basis-DLM_{res_ext}"

        #print("writing:", labels_outpath.name)

        # save the label image using the updated profile which contains
        # the georeference, affine transform, shape, dype, number of bands, etc.
        with rasterio.open(labels_outpath, "w", **profile_gray) as dst:
            # write the numpy array label_gray to the first band of the output image
            dst.write(label_gray, indexes=1)

    return pixel_counts, labels_outpath



def create_bdlm_tiles(storage_path, BDLM_path, BDLM_date, tn_list, be_tiles, custom_shp=None):
    """
    - Create smaller Basis-DLM tiles
    - Return: object_count
        *
    """

    # list all shapefiles in directory
    shape_files = []
    for tn_file in tn_list:
        #shape_files.extend(list(BDLM_path.glob(f'{BDLM_date}_{s}.geojson')))
        shape_files.extend(list(BDLM_path.glob(f'{BDLM_date}/{tn_file}_f.shp')))

    if custom_shp:
        shape_files.extend([custom_shp])
    print(shape_files)

    print(BDLM_path)
    object_count = 0
    for shape_file in shape_files:
        print(shape_file)
        bDLM = gpd.read_file(shape_file)
        be_tiles = be_tiles.to_crs(bDLM.crs)

        for nr in be_tiles.index:
            be_tile = be_tiles.loc[be_tiles.index == nr]

            left = be_tile.total_bounds[0]
            bottom = be_tile.total_bounds[1]
            right = be_tile.total_bounds[2]
            top = be_tile.total_bounds[3]
            
            # TODO make this faster
            area = bDLM.cx[left:right, bottom:top]

            if area.size > 0:

                intersection = gpd.overlay(area, be_tile, how='intersection')

                tn = ntpath.basename(shape_file)[0:5]
                object_count += len(intersection)
                print(f"BE {nr} hat {len(intersection)} Objekte in {tn}")

                if not intersection.empty:
                    outpath = storage_path / nr / "Shapes" / BDLM_date
                    outpath.mkdir(parents=True, exist_ok=True)
                    intersection.to_file(outpath / f"{nr}_{BDLM_date}_{tn}.shp")

    return object_count


def shape_to_tiff(storage_path, tiff_mapping_csv, be8_nr, TN_list, BDLM_date, bucket_credentials=None, scale_to=None):
    """
    - Create smaller Basis-DLM tiles
    - Return: object_count
        *
    """
    #TODO Split this Function un multiple

    # this tif defines the area of interest
    #tif = (storage_path / be8_nr / "S2" / S2_date / f"{be8_nr}_S2_{S2_date}_TCI_10m.tif")
    
    image_list = list(storage_path.glob(f'{be8_nr}/S2/*/{be8_nr}_S2_*_TCI_10m.tif'))
    tif = image_list[0]
    
    # Get content from bucket
    prefix = f'{be8_nr}/S2'
    url = bucket_credentials['url'] + bucket_credentials['bucket'] + f'/?prefix={prefix}&format=json'
    content = pd.read_json(url)

    exit()
    # Get available dates from file names in bucket
    if len(content):
        objects_in_bucket = content.name.to_list()

        pattern = f'{tile_nr}/S2/{date_str}*/{tile_nr}_S2_{date_str}*_TCI_10m.tif'
        image_list = fnmatch.filter(objects_in_bucket, pattern)
    
    # create list of shape files
    shape_dir = storage_path / be8_nr / "Shapes" / BDLM_date

    if os.path.isdir(shape_dir) and os.path.isfile(tif):
        be8_shapefiles = []
        for TN in TN_list:
            be8_shapefiles.extend(list(shape_dir.glob(f"{be8_nr}_{BDLM_date}_{TN}.shp")))

        # read the list of shapefiles
        all_shape_data, schema, shape_crs, driver = read_all_shapes(be8_shapefiles)

        # check for valid geometries
        if all_shape_data:
            shapes, attributes = zip(*((shape(s), p) for s, p in all_shape_data if
                                       s is not None and (s["type"] == "Polygon" or s["type"] == "MultiPolygon")
                                       and isvalid(s)))

            # combine geometries and attributes in a GeoDataFrame
            gdf = gpd.GeoDataFrame(attributes)
            gdf.geometry = list(shapes)
            gdf.crs = shape_crs
            gdf["TiffID"] = np.nan
            gdf["Bezeichnung"] = ""
            gdf["OBJART"] = pd.to_numeric(gdf["OBJART"])
            gdf = gdf.explode()

            # read class mapping file
            df_cm = pd.read_csv(tiff_mapping_csv)

            # assign ClassID and ClassName from df_classmapping -> class_mapping_file_for_landuse.xlsx
            for grp_oba, grp_dat in df_cm.groupby("Objektart"):
                gdf_sel = gdf.OBJART == grp_oba
                if len(grp_dat) == 1:
                    gdf.loc[gdf_sel, "TiffID"] = grp_dat.TiffID.max()
                    gdf.loc[gdf_sel, "Bezeichnung"] = grp_dat.Bezeichnung.max()
                else:
                    for row_idx, row in grp_dat.iterrows():
                        try:
                            #print('value:', row.Value, 'key: ', row.Key)
                            if row.Value == "None":
                                subgrp_sel = gdf[row.Key].apply(lambda x: x is None)
                            else:
                                subgrp_sel = gdf[row.Key] == row.Value
                            gdf.loc[gdf_sel & subgrp_sel, "TiffID"] = row.TiffID
                            gdf.loc[gdf_sel & subgrp_sel, "Bezeichnung"] = row.Bezeichnung
                        except:
                            pass

            gdf = gdf[pd.notna(gdf.TiffID)]
            gdf.TiffID = gdf.TiffID.astype(np.uint8)

            gdf["label_value"] = gdf.TiffID
            # if 0 in gdf.label_value:
            # if 0 is one of the label_values increment all values by 1
            # this is done because 0 is the background color in the label image
            # gdf.label_value += 1

            # here all geometry of the same class are combined by a cascaded_union operation
            class_masks = {}

            # group features in the geodataframe
            for class_id, class_dat in gdf.groupby("TiffID"):
                # generate a multipolygon for each class
                # buffer geometry with 0 buffer to avoid invalid polygons
                class_poly = MultiPolygon([s.buffer(0) for s in class_dat.geometry])

                # run the cascaded_union operation
                class_union = cascaded_union(class_poly)

                class_name = class_dat.Bezeichnung.max()  # get class name as string
                label_value = class_dat.label_value.max()  # get class label

                # store class_name and class_union geometry in the class_masks dictionary
                class_masks[class_id] = (class_id, class_name, label_value, class_union)

            # build a GeoDataFrame from the class_masks dictionary to save them to a shapefile
            gdf_classmasks = gpd.GeoDataFrame(class_masks.values(), index=class_masks.keys(),
                                              columns=["TiffID", "Bezeichnung", "label_value", "geometry"])
            gdf_classmasks.crs = shape_crs

            outpath = storage_path / be8_nr / "Labels" / BDLM_date
            outpath.mkdir(parents=True, exist_ok=True)

            # save the GeoDataFrame to shapefile
            gdf_classmasks.to_file(outpath / f"{be8_nr}_Labels_{BDLM_date}_Basis-DLM.shp")

            #print(outpath)
            pixel_counts, labels_outpath = write_labels_and_masks(tif, gdf_classmasks, outpath, BDLM_date, plot=False, scale_to=scale_to)


            # Upload to COS
            if bucket_credentials:
                cos = storage.Storage(bucket_credentials['url'], 
                                      bucket_credentials['access_key'],
                                      bucket_credentials['secret_key']) 

                bucket_name = bucket_credentials['bucket']
                object_name = f'{be8_nr}/Labels/{BDLM_date}/{labels_outpath.name}'

                cos.store(labels_outpath, object_name, bucket_name)

            pixel_counts = pixel_counts[['TiffID', 'Anzahl']]
            #display(pixel_counts.head())

            df = df_cm.set_index('TiffID').join(pixel_counts.set_index('TiffID'))
            df['Prozent'] = df.Anzahl / df.Anzahl.sum() * 100
            #display(df.head())

            df['Nr'] = df['Objektart'].astype(str).str[:-3].astype(np.int8)

            df.loc[df['Nr'] == 41, 'Objektartengruppe'] = 'Siedlung'
            df.loc[df['Nr'] == 42, 'Objektartengruppe'] = 'Verkehr'
            df.loc[df['Nr'] == 43, 'Objektartengruppe'] = 'Vegetation'
            df.loc[df['Nr'] == 44, 'Objektartengruppe'] = 'Gewässer'

            sns.set()

            colors_24 = ['red', 'orange', 'orange', 'brown', 'grey', 'darkred',
                         'red', 'orange', 'orange', 'brown', 'grey', 'darkred',
                         'yellow', 'indigo', 'lightgreen', 'darkgreen', 'green', 'purple',
                         'brown', 'brown', 'lightblue', 'lightblue', 'blue', 'darkblue']

            colors_4 = ['blue', 'red', 'green', 'yellow']

            df_sum = df.groupby('Objektartengruppe').sum()

            fig = plt.figure(figsize=(14, 6))

            st = fig.suptitle(f"BE-Kachel: {be8_nr} {BDLM_date}", fontsize="x-large")

            ax1 = fig.add_subplot(132)
            ax2 = fig.add_subplot(133)

            ax = df.plot.barh(ax=ax1, x='Bezeichnung', y='Anzahl', color=colors_24)
            ax1.invert_yaxis()
            ax1.set_xlabel('Anzahl 10m x 10m Pixel')
            ax1.set_xlim(0, 40000)
            ax1.get_legend().remove()

            ax = df_sum.plot.pie(ax=ax2, y='Prozent', colors=colors_4, labels=None)
            plt.legend(labels=df_sum.index, loc="upper left")


            TN_stats_plot_path = storage_path / be8_nr / "Labels" / f"TN_{be8_nr}_{BDLM_date}.png"
            fig.savefig(TN_stats_plot_path)

            plots_path = storage_path / 'plots'
            plots_path.mkdir(parents=True, exist_ok=True)
            fig.savefig(plots_path / f"TN_{be8_nr}_{BDLM_date}.png")

            plt.close(fig)


def add_overlay_layer(storage_path, be8_nr, tn_list, overlay_list, BDLM_date):
    # Overlay Basis-DLM tiles by identity_list
    
    for layer in tqdm(tn_list, desc=f'Adding ovelay for {be8_nr}'):
        #print(layer)
        for overlay in overlay_list:
            
            shape_dir = storage_path / be8_nr / "Shapes" / BDLM_date
            
            if os.path.isdir(shape_dir):
                
                layer_shp = Path(f'{shape_dir}/{be8_nr}_{BDLM_date}_{layer}.shp')
                overlay_shp = Path(f'{shape_dir}/{be8_nr}_{BDLM_date}_{overlay}.shp')
                
                # name example: veg03 + veg 04 -> veg03_veg04
                out_filename = f'{be8_nr}_{BDLM_date}_{layer}_{overlay}'
                outpath = storage_path / be8_nr / "Shapes" / BDLM_date

                # read the shapefiles
                if not layer_shp.exists():
                    break
                else:
                    if overlay_shp.exists():
                        # read shapefiles
                        layer1 = gpd.read_file(layer_shp)
                        layer2 = gpd.read_file(overlay_shp) # veg04

                        # remove column from second shapefile to get attribute name OBJART (not OBJART_1)
                        layer2 = layer2.drop(columns=['OBJART'])

                        # remove objects with ZUS = 5000 (not necessary and produces mistakes) and remove column ZUS
                        layer2 = layer2[layer2['ZUS'] != "5000"] #"nass" (Feldtyp: text!!)
                        #layer2 = layer2.drop(columns=['ZUS'])

                        # union shapefiles (layer1 & layer2)

                        try:
                            identity = layer1.overlay(layer2, how='identity')
                            
                            if not identity.empty:
                                identity.to_file(outpath / f"{out_filename}.shp")
                            else: # is empty
                                print('empty')
                                layer1['BWS'] = ''
                                layer1.to_file(outpath / f"{out_filename}.shp")    
                            
                        except pd.errors.IntCastingNaNError: # catch pandas error in geopandas version 0.10.0
                            print('error')
                            layer1['BWS'] = ''
                            layer1.to_file(outpath / f"{out_filename}.shp")
                            

                    #if overlay_shp not exist:
                    else:
                        print('no file')
                        layer1 = gpd.read_file(layer_shp)
                        
                        layer1['BWS'] = ''
                        layer1.to_file(outpath / f"{out_filename}.shp")
                        
        
        
def hdu(storage_path, be8_nr, hdu_list, BDLM_path, BDLM_date, tn_list):
    
    # Entfernen von Geometrien, die HDU_X (hatDirektUnten_existiert) Relationen haben und unterhalb der oberen Ebene zur Klassifizierung liegen

    # create list of shape files
    shape_dir = storage_path / be8_nr / "Shapes" / BDLM_date

    if os.path.isdir(shape_dir):
        hdu_shapes = []
        for hdu in hdu_list:
            hdu_shapes.extend(list(shape_dir.glob(f"{be8_nr}_{BDLM_date}_{hdu}.shp")))

        # read the list of shapefiles
        all_shape_data, schema, shape_crs, driver = read_all_shapes(hdu_shapes)
        # check for valid geometries
        bauwerke_dict = {}
        
        if all_shape_data:
            shapes, attributes = zip(*((shape(s), p) for s, p in all_shape_data if
                                       s is not None and (s["type"] == "Polygon" or s["type"] == "MultiPolygon")
                                       and isvalid(s)))


            # read shapefiles hdu01 und ver06 (Bauwerke)
            hdu_path = f"{BDLM_path}/{BDLM_date}/hdu01_b.shp"
            bauw_path =  f"{BDLM_path}/{BDLM_date}/ver06_f.shp"
            hdu = gpd.read_file(hdu_path)
            ver06 = gpd.read_file(bauw_path)

            # Umwandeln der GeoDataFrames in DataFrames und löschen der 'geometry' columns, weil mehrere 'geometry' columnns zu Problemen geführt haben
            hdu = pd.DataFrame(hdu.drop(columns='geometry'))
            ver06 = pd.DataFrame(ver06.drop(columns='geometry'))


            

            for hdu_shape in hdu_shapes:

                # read shapefile
                layer = gpd.read_file(hdu_shape)

                # Join der Objekte mit HDU_X-Relation
                join = layer.merge(hdu, how='left', left_on='OBJID', right_on='OBJID_1', suffixes=(None, "_y"))
                join = join.merge(ver06, how='left', left_on='OBJID_2', right_on='OBJID', suffixes=(None, "_y"))

                # Selektion der obenliegenden Objekte
                selection = join[join['BWF'] != "1870"] #Tunnel
                selection = selection[selection['BWF'] != "2012"] #Düker
                selection = selection[selection['BWF'] != "2050"] #Wehr
                selection = selection[selection['BWF'] != "2070"] #Siel
                selection = selection[selection['BWF'] != "2080"] #Sperrwerk
                selection = selection[selection['BWF'] != "2090"] #Schöpfwerk                

                bauwerke = selection[selection['HDU_X'] == 1] # alle Bauwerke selektieren (die noch übrig sind, also oben liegen)

                if not bauwerke.empty:                
                    key = ntpath.basename(hdu_shape)[20:25]
                    bauwerke_dict[key] = bauwerke  

        # Import aller BDLM-shapes der Kachel

        if os.path.isdir(shape_dir):
            tn_shapes = []
            for tn in tn_list:
                tn_shapes.extend(list(shape_dir.glob(f"{be8_nr}_{BDLM_date}_{tn}.shp")))

            # read the list of shapefiles
            all_shape_data, schema, shape_crs, driver = read_all_shapes(tn_shapes)

            # check for valid geometries
            if all_shape_data:
                shapes, attributes = zip(*((shape(s), p) for s, p in all_shape_data if
                                           s is not None and (s["type"] == "Polygon" or s["type"] == "MultiPolygon")
                                           and isvalid(s)))


                # erase der BDLM-Shapes mit den Bauwerken
                if tn_shapes:
                    for tn_shape in tqdm(tn_shapes, desc=f'Resolve bridges {be8_nr}'):
                        layer = gpd.read_file(tn_shape)
                        if bauwerke_dict:
                            for bauwerke in bauwerke_dict.values():
                                bauwerke = bauwerke.dissolve(by='OBJART_1')
                                layer = gpd.overlay(layer, bauwerke, how='difference')

                            # Selektion der obenliegenden Objekte aus den neuimportierten Shapes
                            # zuerst: column-names vorbereiten für die HDU_X-Tabellenverknüpfung
                            if 'OBJID_1' in layer.columns:
                                if 'OBJID' in layer.columns:
                                    layer = layer.drop(columns=['OBJID'])
                                layer = layer.rename(columns={'OBJID_1': 'OBJID'})              

                            # HDU_X-Relationen herstellen
                            layer = layer.merge(hdu, how='left', left_on='OBJID', right_on='OBJID_1', suffixes=("_a", None))
                            layer = layer.merge(ver06, how='left', left_on='OBJID_2', right_on='OBJID', suffixes=(None, "_b"))

                            # Selektion der obenliegenden Layer (Tunnel etc. entfernen)
                            layer = layer[layer['BWF'] != "1870"] #Tunnel
                            layer = layer[layer['BWF'] != "2012"] #Düker
                            layer = layer[layer['BWF'] != "2050"] #Wehr
                            layer = layer[layer['BWF'] != "2070"] #Siel
                            layer = layer[layer['BWF'] != "2080"] #Sperrwerk
                            layer = layer[layer['BWF'] != "2090"] #Schöpfwerk


                            # Zusammenfügen der Shapes mit den jeweiligen Bauwerken, wenn 'tn_shape' Verkehr oder Gewässer ist
                            if ntpath.basename(tn_shape)[20:23] == "ver" or ntpath.basename(tn_shape)[20:23] == "gew":
                                for bauwerke in bauwerke_dict.keys():
                                    if bauwerke == ntpath.basename(tn_shape)[20:25]:
                                        #print(f"{ntpath.basename(tn_shape)[20:25]} == {bauwerke}")

                                        # delete duplicate columns; sonst funktioniert append nicht...
                                        df = bauwerke_dict[bauwerke].loc[:,~bauwerke_dict[bauwerke].columns.duplicated()]
                                        try:
                                            layer = layer.append(df)
                                        except:
                                            pass


                        # save shapefile in the same be8 folder with ending "_NO"
                        # möglicherweise ist es schlau, die orginal Datei zu überschreiben...
                        main_name = ntpath.basename(tn_shape)[0:25]
                        ending = "_n"
                        filename = main_name + ending
                        #print(filename)
                        nr = ntpath.basename(tn_shape)[0:10]

                        if not layer.empty:
                            outpath = storage_path / nr / "Shapes" / BDLM_date
                            outpath.mkdir(parents=True, exist_ok=True)
                            layer.to_file(outpath / f"{filename}.shp")



def imagery_of_classes(storage_path, be8_nr, BDLM_date, S2_date):
    
    # create single S2 imagery files for every class and every geometry of the BDLM-classification per BE8 tile
    
    # read label shapefile
    labelpath = storage_path / be8_nr / "Labels" / BDLM_date / f"{be8_nr}_Labels_{BDLM_date}_Basis-DLM.shp"
    labels_gdf = gpd.read_file(labelpath)
    
    # iterate over GDF
    for index, row in labels_gdf.iterrows():
        print(f"Klasse {row['TiffID']} ({row['Bezeichnun']})")
        
        # select row for every iteration
        row_gdf = labels_gdf[labels_gdf['TiffID'] == row['TiffID']]
        row_gdf.plot()
        
        # open raster
        S2path = storage_path / be8_nr / "S2" / S2_date / f"{be8_nr}_S2_{S2_date}_TCI_10m.tif"
        S2_raster = rasterio.open(S2path)
        
        rasterio.plot.show(S2_raster)
        
        print(f"shape CRS {row_gdf.crs}")
        print(f"raster CRS {S2_raster.crs}")
        
        # reproject GDF to raster CRS (both rows work, but what is the better way?)
        row_gdf = row_gdf.to_crs(f"{S2_raster.crs}")
        #row_gdf = row_gdf.to_crs(crs=S2_raster.crs)
        
        print(f"new shape CRS {row_gdf.crs}")
        row_gdf.plot()
        
        # multipolygon to singlepart polygons
        single_gdf = row_gdf.explode()
        single_gdf = single_gdf.reset_index(drop=True)
        single_gdf['singleID'] = single_gdf.index
        
        # iterate over singlepart polygons GDF (for imagery of objects seperatly)
        for i, polygon in single_gdf.iterrows():
            # select row (singlepart polygon) for every iteration
            singlepart = single_gdf[single_gdf['singleID'] == i]
            #print(singlepart)
            
            # clip raster with 'singlepart' GDF...
            # easily put all following lines into this loop and modify file name with added 'singleID'
        
        
        # extract geometry from gdf (-> GeoSeries)
        geometry = row_gdf['geometry']
        print(geometry)

        # TODO: add alpha-channel to raster for avoid black frame
        #mask the S2 image
        out_image, out_transform = rasterio.mask.mask(S2_raster, geometry, crop=True)
        out_meta = S2_raster.meta
        rasterio.plot.show(out_image)
        
        out_meta.update({"driver": "GTiff",
                         "height": out_image.shape[1],
                         "width": out_image.shape[2],
                         "transform": out_transform})
        
        # save imagery of class as tiff
        outpath = storage_path / be8_nr / "ClassIMG" / BDLM_date
        outpath.mkdir(parents=True, exist_ok=True)
        outpath = storage_path / be8_nr / "ClassIMG" / BDLM_date / f"{be8_nr}_ClassImg{row['TiffID']}.tif"
        
        with rasterio.open(outpath, "w", **out_meta) as dest:
            dest.write(out_image)
        
        # save as jpg for preview
        outpath = storage_path / be8_nr / "ClassJPGs" / BDLM_date
        outpath.mkdir(parents=True, exist_ok=True)
        outpath = outpath / f"{be8_nr}_ClassImg{row['TiffID']}.jpg"
        
        out_meta.update({"driver": "JPEG",
                        "height": out_image.shape[1],
                        "width": out_image.shape[2],
                        "transform": out_transform} # TODO remove to avoid aux files
                       )
            
        with rasterio.open(outpath, "w", **out_meta) as output:
            output.write(out_image)
        
        # just create images from first 3 classes
        if index == 2:
            break
    

    
    
    
        # TiffID = ID der Klasse
        # singleID = laufende nummer der singlepart geometrien -> column 'sinlgeID'
        # (outpath / f"{be8_nr}_ClassIMG{TiffID}_{singleID}.tif")
        
        
