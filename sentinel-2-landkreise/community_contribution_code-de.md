# Sentinel-2-Landkreise
Täglich aktualisierte, wolkenfreie Sentinel-2-Daten, zugeschnitten für jeden Landkreis Deutschlands. Ab jetzt dauerhaft auf CODE-DE verfügbar.

Die hohe Aktualität der Sentinel-2-Daten macht diese auch für Anwendungen auf Landkreisebene interessant. In vielen Bereichen werden die Daten bisher aber nicht genutzt, da der Zugriff oder die Auswahl wolkenfreier Aufnahmen zu kompliziert ist. 

Das Produkt “Sentinel-2-Landkreise” ist ein Prototyp der geoLabs des Landesamt für Geoinformation und Landesvermessung Niedersachsen (LGLN). Es vereinfacht die Verwendung der Sentinel-2-Daten und unterstützt die Auswahl wolkenfreier Aufnahmen. Es wird dauerhaft, frei verfügbar angeboten und ständig weiterentwickelt.

## Verfügbarkeit
- Deutschland: Vollständige Abdeckung
- Europa: in Planung
- Weltweit: -

## Aktualisierung
- täglich (im Zeitraum 20:00 - 24:00 Uhr)

## Datenzugriff
- URL zu den Daten: https://lgln.community.code-de.org/sentinel-2-landkreise/
- Beispiel zum Filtern der Daten: Landkreis Lüneburg https://lgln.community.code-de.org/sentinel-2-landkreise/?prefix=Luen
- Jupyter Notebook mit weiteren Beispielen zum Datenzugriff mit Python: https://gitlab.opencode.de/lgln/sentinel2l
- STAC-API: Demnächst verfügbar
- EO Browser: -
- EO Finder: -

## Datenformat
Die Sentinel-2-Landkreise werden als [Cloud Optimized GeoTiffs](https://www.cogeo.org/) bereitgestellt. Sie lassen sich direkt, ohne Download in GIS-Anwendungen darstellen.

## Weiterführende Informationen
[Aufzeichnung des Vortrags "Sentinel-2-Landkreise" beim Webinar: CODE-DE für kommunale Anwendungen am 08. Februar 2024](https://www.youtube.com/watch?v=mtM_D8Hk7EQ)

## Source Code
https://gitlab.opencode.de/lgln/sentinel2l

## Haftungshinweis:
Bei dem Produkt “Sentinel-2-Landkreise” handelt es sich um einen Prototyp aus den geoLabs des LGLN. Es ist kein offizielles, amtliches Produkt des LGLN. Es gibt keine Garantie für Vollständigkeit, Korrektheit und dauerhafte Verfügbarkeit der Daten. Auch wenn wir ständig an der Verbesserung des Produktes arbeiten, sollte es für kritische Anwendungen derzeit nicht produktiv verwendet werden.

## Lizenz
[CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/)
"Enthält modifizierte Copernicus [Sentinel-Daten](https://sentinel.esa.int/documents/247904/690755/Sentinel_Data_Legal_Notice) [Jahr], verarbeitet am Landesamt für Geoinformation und Landesvermessung Niedersachsen (LGLN)"

## Impressum
https://lgln.community.code-de.org/
