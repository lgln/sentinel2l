# Sentinel-2-Landkreise
Daily updated, cloud-free Sentinel-2 data tailored for each district (Landkreis) in Germany. Now permanently available on CODE-DE.
The high timeliness of Sentinel-2 data also makes it interesting for applications at the district level. However, in many areas, the data is not yet utilized because accessing or selecting cloud-free images is too complicated.
The "Sentinel-2-Landkreise" is a prototype developed by the geoLabs of the State Office of Lower Saxony for Geoinformation and Surveying (LGLN). It simplifies the use of Sentinel-2 data and supports the selection of cloud-free images. It is offered permanently, freely available, and will be improved continuously.

## Availability

- Germany: Full coverage
- Europe: Planned
- Worldwide: -

## Updates

- Daily (between 20:00 - 24:00 UTC)

## Access

- URL to the access the data: https://lgln.community.code-de.org/sentinel-2-landkreise/

- Example for filtering the data: Lüneburg district https://lgln.community.code-de.org/sentinel-2-landkreise/?prefix=Luen

- Jupyter Notebook with further examples for data access with Python: https://gitlab.opencode.de/lgln/sentinel2l

- STAC API: Coming soon
- EO Browser: -
- EO Finder: -

## Data Format
The Sentinel-2-Landkreise are provided as [Cloud Optimized GeoTiffs](https://www.cogeo.org/). They can be directly displayed in GIS applications without downloading.

## More  Information
[Recording of the presentation "Sentinel-2-Landkreise" (in german) at the webinar: CODE-DE for municipal applications on February 8, 2024](https://www.youtube.com/watch?v=mtM_D8Hk7EQ)

## Source Code
https://gitlab.opencode.de/lgln/sentinel2l

## Disclaimer
The product "Sentinel-2-Landkreise" is a prototype from the geoLabs of the LGLN. It is not an official, authoritative product of the LGLN. There is no guarantee of completeness, correctness, and permanent availability of the data. Although we continuously work on improving the product, it should not be used for critical applications at this time.

## Terms of Use
[CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/)
"Contains modified Copernicus [Sentinel data](https://sentinel.esa.int/documents/247904/690755/Sentinel_Data_Legal_Notice) [Year], processed by the State Office of Lower Saxony for Geoinformation and Surveying (LGLN)"

## Imprint / Impressum
https://lgln.community.code-de.org/
