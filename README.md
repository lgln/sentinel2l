# Sentinel2L

**EN**: This tool aims to facilitate access to cloud-free Sentinel-2 satellite imagery. The target audience are German authorities. For this reason, the documentation is in German. The code is mostly in English.

**DE**: Dieses Tool soll den Zugang zu wolkenfreien Sentinel-2-Satellitenaufnahmen erleichtern. Das Zielpublikum sind deutsche Behörden. Aus diesem Grund ist die Dokumentation auf Deutsch. Der Code ist größtenteils in Englisch.

Sentinel2L (*Sentinel-Tool*) ist Teil des Produkt-Prototypen "Sentinel-2-Landkreise" und wird vom [geoLabs-Team "KI"](geolab_team_ki@lgln.niedersachsen.de) des [LGLN](www.lgln.niedersachsen.de) entwickelt. Es umfasst Python-Funktionen und Jupyter-Notebooks zum Abrufen und Erstellen wolkenfreier Sentinel-2-Satellitenaufnahmen.

## Jupyter Notebooks:
- [Sentinel-2-Landkreise](Sentinel-2-Landkreise.ipynb) Abrufen fertiger Sentinel-2-Landkreise
- [Start-Mosaicing]() coming soon
- [Sentinel-2-Tiles]() coming soon
- [Start-Tiling]() coming soon
- [Import-Landkreise]() coming soon


## Nutzung der Jupyter Notebook auf der CODE-DE-Cloud

1. Registrieren und Anmelden auf https://code-de.org
2. Jupyter Lab starten unter https://jupyterlab.code-de.org
3. Im Launcher ein neues Terminel starten
4. Das Sentinel2l-Repository mit git clonen:
```
git clone https://gitlab.opencode.de/lgln/sentinel2l.git
```
5. Im Juypter Lab File-Brwoser den Ordner "sentinel2l" öffnen
6. Jupyter Notebook starten (z.B. Sentinel-2-Landkreise.ipynb)


## Haftungshinweis
Bei dem Produkt “Sentinel-2-Landkreise” handelt es sich um einen Prototyp aus den geoLabs des LGLN. Es ist kein offizielles, amtliches Produkt des LGLN. Es gibt keine Garantie für Vollständigkeit, Korrektheit und dauerhafte Verfügbarkeit der Daten. Auch wenn wir ständig an der Verbesserung des Produktes arbeiten, sollte es für kritische Anwendungen derzeit nicht produktiv verwendet werden.
